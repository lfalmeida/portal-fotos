<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Pedido_dao_model extends CI_Model  
{

	const     TABLE_NAME = 'pedidos',
				   	  PK = 'id'; // Primary Key
	/**
     *
     *
     *
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Cria ou altera um registro, caso o
	 * objeto tenha ou não um atributo 'id'
	 *
	 */
	public function save($pedido)
	{
		$this->pedido = $pedido;

		$data = array(
					'cliente_id' => $pedido->getCliente(),
					'obs_pedido' => $pedido->getObs(),
					'obs_pedido_final' => $pedido->getObsFinal(),
				);

		if( $pedido->getId()) // alterar registro
		{
			return self::update($data);
		}
		return self::insert($data);
	}


	/**
	 * Insere um registro no banco
	 *
	 *
	 */
	private function insert($data)
	{
		$data['data_criacao'] = date('Y-m-d H:i:s', time());
		if( $this->db->insert(self::TABLE_NAME, $data) ) {
			$pedidoId = $this->db->insert_id();
			try{
				$this->saveAnexos($pedidoId);
				$this->saveServicos($pedidoId);
				$this->saveValorTotal($pedidoId);
				return $pedidoId;
			} catch(Exception $e) {
				throw $e;
				return false;
			}
		}
		return false;
	}

	/**
	 * Altera um registro do banco
	 *
	 *
	 */
	private function update($data)
	{
		$this->db->where(self::PK, $data[self::PK] );

		if( $this->db->update(self::TABLE_NAME, $data) ) {
			return true;
		}
		return false;
	}


	/**
	 * Remove um registro do BD
	 *
	 *
	 */
	public function delete( $identifier )
	{
		return $this->db->delete(self::TABLE_NAME, array(self::PK => $identifier ) );
	}


	/**
	 * Obtem registros do banco de dados
	 * de acordo com as opções fornecidas
	 *
	 */
	public function getAll(array $options = array())
	{
		$this->processQueryOptions($options);

        $result = $this->db->get(self::TABLE_NAME)->result();

        return $this->build($result);
	}


    /**
     *
     *
     */
	private function processQueryOptions(array $options = array()) {

        if( isset( $options['order'] ) AND null != $options['order'] ) {
            $this->db->order_by($options['order']);
        }

        if( isset( $options['where'] ) AND null != $options['where'] ) {
            $this->db->where($options['where']);
        }

        if( isset($options['limit']) AND isset($options['offset']) ) {
            $this->db->limit($options['limit'], $options['offset']);
        }

	}


	/**
	 * Obtem um registro do banco de acordo com
	 *  o identificador informado
	 *
	 *
	 *
	 */
	public function find($identifier,  $returnObject = true)
	{
		// chave primária
		if(is_numeric($identifier))
		{
			$this->db->where(self::PK, $identifier);
			$result = $this->db->get(self::TABLE_NAME)->result();
			return $this->build($result, $returnObject);
		}

        return false;
	}

	/**
	 * Retorna pedidos do cliente
	 * @param int $clienteId
	 *
	 */
	public function getPedidosByClienteId($clienteId, $options = array()) {

		$this->processQueryOptions($options);

		$this->db->where('cliente_id', $clienteId);
		$result = $this->db->get('pedidos')->result();
		return $this->build($result);
	}

	/**
	 * Retorna anexos de um Pedidos
	 * @param int $pedidoId
	 *
	 */
	public function getAnexosByPedidoId($pedidoId, $options = array()) {
		$this->processQueryOptions($options);
		return $this->db->get_where('anexos_pedido', array('pedido_id' => $pedidoId))->result();
	}

	/**
	 * Retorna anexos Originais, (não tratados) de um Pedidos
	 * @param int $pedidoId
	 *
	 */
	public function getAnexosOriginaisByPedidoId($pedidoId, $options = array()) {
		return $this->db->get_where('anexos_pedido', array('pedido_id' => $pedidoId, 'tipo' => 'O'))->result();
	}

	/**
	 * Retorna anexos Originais, (não tratados) de um Pedidos
	 * @param int $pedidoId
	 *
	 */
	public function getAnexosFinalizadosByPedidoId($pedidoId, $options = array()) {
		return $this->db->get_where('anexos_pedido', array('pedido_id' => $pedidoId, 'tipo' => 'F'))->result();
	}

	/**
	 * Retorna um array de objetos servico de um Pedido
	 * @param int $pedidoId
	 * @return array $servicos 
	 */
	public function getServicosByPedidoId($pedidoId) {
		$CI =& get_instance();
		$servicos = array();
		$CI->load->model('servico_dao_model', 'servicosRepository');
		$servicos = $CI->servicosRepository->getServicosByPedidoId($pedidoId);
		return $servicos;
	}


	/**
	 *
	 * Constrói e retorna objetos ou array de objetos da tabela
	 * Recebe um objeto ou array de objetos
	 * resultante de uma consulta ao BD.
	 * Pode retornar um objeto ou um array de objetos.
	 *
	 */
	public function build ( $data, $returnObject = false )
	{
		if( is_array($data) ) {
			$pedidos = array();
			foreach ($data as $row){
				$pedido	= new Pedido();
				$pedido->setId($row->id)
					   ->setCliente($row->cliente_id)
				   	   ->setObs($row->obs_pedido)
					   ->setObsFinal($row->obs_pedido_final)
					   ->setStatus($row->status)
					   ->setAnexos($this->getAnexosByPedidoId($row->id))
					   ->setServicos($this->getServicosByPedidoId($row->id))
					   ->setDataCriacao($row->data_criacao)
					   ->setDataAtualizacao($row->data_ultima_atualizacao)
                       ->setDescricao($row->descricao)
                       ->setValorTotal($row->valor_total)
                       ->setPagseguroCheckoutCode($row->pagseguro_checkout_code);
				$pedidos[] = $pedido;
			}

			if( count($pedidos) == 1 &&  $returnObject != false ) {
				return $pedidos['0'];
			}
			return empty($pedidos) ? false : $pedidos;
		}
		return false;
	}


    public function savePagseguroCheckoutCode($pedidoId, $code) {
            $this->db->where('id',$pedidoId);
            $this->db->update('pedidos',array( 'pagseguro_checkout_code' => $code));
    }

	/**
	 *
	 */
	protected function saveAnexos( $pedidoId ) {
		$anexos = $this->pedido->getAnexos();
		if( ! is_array($anexos) || empty($anexos) ) {
			throw new Exception('Não foi possível salvar o pedido sem fotos', 1);
			return false;
		}

		$data = array();
		foreach($anexos as $anexo) {
			$data[] = array(
				'anexo' => $anexo,
				'tipo' => 'O', //Original
				'pedido_id' => $pedidoId
			);
		}

		$this->db->insert_batch('anexos_pedido', $data);
	}

	/**
	 *
	 */
	protected function saveServicos($pedidoId) {
		$servicos = $this->pedido->getServicos();
		if( ! is_array($servicos) || empty($servicos) ) {
			throw new Exception('Não foi possível salvar o pedido sem serviços', 1);
			return false;
		}

		$data = array();
		foreach($servicos as $servico) {
			$data[] = array(
				'servico_id' => $servico,
				'pedido_id' => $pedidoId,
			);
		}
		$this->db->insert_batch('servicos_pedido', $data);
	}

    /**
     * Calcula e salva no banco o valor total e uma
     * descricao baseada nos arquivos e servicos contratados
     *
     */
 	private function saveValorTotal($pedidoId) {

 	    $CI =& get_instance();
        $CI->load->model('servico_dao_model', 'servicosRepository');
        $totalAnexos = count($this->pedido->getAnexos());
        $servicosIds = $this->pedido->getServicos();

        $valorServicos = 0;
        $nomesServicos = array();
        if(is_array($servicosIds) && !empty($servicosIds)) {
            foreach ($servicosIds as $key => $servicoId) {
                $servico = $CI->servicoRepository->find($servicoId);
                if(is_object($servico)){
                    $valorServicos += $servico->getPreco(true);
                    $nomesServicos[] = $servico->getNome();
                }
            }
        }

        $description = 'Tratamento de ' . $totalAnexos . ' foto(s) ' .
                       'com os seguintes serviços: ' . implode(',', $nomesServicos);

        $totalPedido = ($valorServicos * $totalAnexos);
 		$this->db->where('id', $pedidoId);
        $this->db->update('pedidos', array('valor_total' => $totalPedido, 'descricao' => $description ));
 	}


    /**
     *
     *
     *
     */
    public function finalizarPedido($pedidoId, $anexos = array(), $obsFinal) {
        if(empty($anexos)) {
            throw new Exception("Não é possível finalizar um pedido sem anexar os arquivos finalizados", 1);
            return false;
        }
        // salvando os anexos finalizados
        $data = array();
        foreach($anexos as $anexo) {
            $data[] = array(
                'anexo' => $anexo,
                'tipo' => 'F', //Finalizados

                'pedido_id' => $pedidoId
            );
        }
        $this->db->insert_batch('anexos_pedido', $data);

        // alterando o status do pedido
        $this->db->where('id', $pedidoId);
        $this->db->update('pedidos', array('status' => 'F', 'obs_pedido_final' => $obsFinal ) );
        return true;
    }


    /**
     *
     *
     *
     */
     public function updateStatus($pedidoId, $status) {
         $this->db->where('id', $pedidoId);
         return $this->db->update('pedidos', array('status' => $status));
     }


     public function buscarPedidos($conditions) {

     }


}
