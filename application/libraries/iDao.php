<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


interface iDAO
{

	/**
	 * Cria ou altera um registro, caso o
	 * objeto tenha ou não um atributo 'id'
	 *
	 */
	public function save( $object );

	/**
	 * Remove um registro do BD
	 *
	 *
	 */
	public function delete( $identifier );

	/**
	 * Obtem registros do banco de dados
	 * de acordo com as opções fornecidas
	 *
	 */
	public function getAll( array $options = array() );

	/**
	 * Obtem um registro do banco, dependendo
	 * do identificador passado como parâmetro
	 *
	 */
	public function find ( $identifier );

	/**
	 *
	 * Constrói objetos da classe em questão.
	 * Recebe um objeto ou array de objetos
	 * resultante de uma consulta ao BD.
	 * Pode retornar um objeto ou um array de objetos.
	 *
	 */
	public function build ( $data, $returnObject = false );


}