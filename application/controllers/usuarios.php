<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Usuarios extends Admin_Controller
{
	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('usuario_dao_model', 'userRepository');
	}


    /**
     *
     **/
	public function index()
	{
        $this->gerenciar(0);
	}


	/**
	 * os usuários cadastrado por este método são administradores	  
	 */
	public function cadastrar() 
	{
		if($this->input->post('submit')) {
			$usuario = new Usuario();
			$usuario->setNome($this->input->post('nome', true))
				    ->setSobrenome($this->input->post('sobrenome', true))
					->setEmail($this->input->post('email', true))
					->setSenha($this->input->post('senha', true))
					// define como admin
					->setAdminPerm(1); 
			try {
				$result = $this->userRepository->save($usuario);
				if($result) {
					$message = array(
						'type' => 'success',
						'text' => 'Cadastro realizado com sucesso.'
					);
                    
                    Logger::info('Novo usuário (admin) cadastrado: ' . $usuario->getNome());
                    
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar(); 
                    return true;  
				}				
			} catch(Exception $e) {
                    $message = array(
                        'type' => 'danger',
                        'text' => $e->getMessage()
                    );
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar(); 
                    return false;  
			}
		}
 		$this->load->view('public/usuarios/cadastrar');		
	} 


    /**
     * 
     */
    public function gerenciar($pag = 0) {
            
        $isAdmin = "admin = '1'";
        
        $this->load->library('pagination');
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url() . 'usuarios/gerenciar/';
        $config['total_rows'] = count($this->userRepository->getAll(array('where'=> $isAdmin)));
        $config['per_page'] = 12;
        $this->pagination->initialize($config);
        $paginacao = $this->pagination->create_links();

        $limit = $config['per_page'];
        $offset = $this->uri->segment(3);
        $order = 'id DESC';
        $usuarios = $this->userRepository->getAll(array(
            'where'=> $isAdmin,
            'limit' => $limit,
            'offset' => $offset,
            'order' => $order
                )
        );
        $this->load->vars('usuarios', $usuarios);
        $this->load->vars('paginacao', $paginacao);
        $this->load->vars('currentPage', floor(($this->uri->segment(3) / $config['per_page']) + 1));
        $this->load->view('public/usuarios/gerenciar');
    }

    /**
     * 
     * 
     */
    public function alterar($usuarioId = false ) {
            
        if($this->input->post('submit')) {
            $usuarioId = $this->input->post('usuario_id');
            $usuario = new Usuario();
            $usuario->setId($usuarioId)
                    ->setNome($this->input->post('nome', true))
                    ->setSobrenome($this->input->post('sobrenome', true))
                    ->setEmail($this->input->post('email', true))
                    ->setSenha($this->input->post('senha', true))
                    // define como admin
                    ->setAdminPerm(1); 
            try {
                $result = $this->userRepository->save($usuario);
                if($result) {
                    $message = array(
                        'type' => 'success',
                        'text' => 'Cadastro alterado com sucesso.'
                    );
                    
                    Logger::info('Usuário (admin) alterado: ' . $usuario->getNome());
                    
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar(); 
                    return true;                  
                }               
            } catch(Exception $e) {
                echo $e->getMessage();
            }
        }
         
        $usuario = $this->userRepository->find(($usuarioId));
        $this->load->vars('usuario', $usuario);
        $this->load->view('public/usuarios/alterar');
        
    }


    /**
     * 
     * 
     */
    public function excluir($usuarioId = false) {
        if($this->input->post('submit')) {
            $usuarioId = $this->input->post('usuario_id');
            try {
                $result = $this->userRepository->excluir($usuarioId);
                 if($result) {
                    $message = array(
                        'type' => 'success',
                        'text' => 'Cadastro excluído com sucesso.'
                    );
                    Logger::info('Usuário (admin) excluído: ' . $usuarioId);
                    
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar(); 
                    return true; 
                 }                
            } catch (Exception $e) {
                    $message = array(
                        'type' => 'error',
                        'text' => $e->getMessage()
                    );
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar(); 
                    return true;                 
            }
            
        }
        $usuario = $this->userRepository->find(($usuarioId));
        $this->load->vars('usuario', $usuario);
        $this->load->view('public/usuarios/confirmar_exclusao');
    }

}
