<?php

class Facebooklib {
	
	private $CI;
	private $scope = array('scope' => 'email, user_photos');
	
	/**
	 * 
	 */
	public function __construct() {
		$this->CI =& get_instance(); 		
		$config = array('appId'  => $this->CI->config->item('facebookAppID'), 
						'secret' => $this->CI->config->item('facebookAppSecret')
						);
		// carrega a sdk do facebook 									
		$this->CI->load->library('Facebook/facebook', $config);		
		$this->facebook = $this->CI->facebook;
		
	}
	
	/**
	 * 
	 */
	public function dumpSessionData()  {
		$user = $this->facebook->getUser();

 		if($user) {
			try {
	 			$user_profile = $this->facebook->api('/me');
				var_dump($user_profile); 	
				var_dump($_SESSION);
			} catch (FacebookApiException $e) {
				var_dump($e);
			}
 		} else {
 			echo anchor($this->facebook->getLoginUrl($this->scope), 'login');
 		}		
	}

	/**
	 * 
	 */
	public function getUserId() {
		return $this->facebook->getUser();
	}
	
	/**
	 * 
	 */
	public function getUser() {
		try {
 			$me = $this->facebook->api('/me');
			if($me) {
				return $me;
			}
			return false;
		} catch (FacebookApiException $e) {
			// $e;
			return false;
		}
	}

	/**
	 * 
	 */
	public function login() {
		$user = $this->getUser();
		if($user) {
			$this->saveUserData($user);
			return true;
		}	
		return false;
	}
 	
	/**
	 * 
	 */
	 public function saveUserData($user)
	 {
	 	$this->CI->load->model('usuario_dao_model', 'usersRepository');
		$this->CI->usersRepository->saveUserFromFacebook($user);
		$this->CI->usersRepository->setSessionData($user);
	 }
	
	/**
	 * 
	 */	
	public function getLoginUrl() {
		return $this->CI->facebook->getLoginUrl($this->scope);
	}

	/**
	 * 
	 */	
	public function getLogoutUrl() {
		return $this->CI->facebook->getLogoutUrl();
	}
	
	/**
	 * 
	 */	
	public function getUserAlbuns() {
		return $this->facebook->api('/me/albums');
	}
	
	/**
	 * 
	 */	
	public function getAlbumPhotos($albumId) {
		return $this->facebook->api( $albumId . '/photos');
	}

	/**
	 * 
	 */
	public function getPhotoUrl($photoId) {
		return $this->facebook->api('/' . $photoId);
	}
	
	
}
