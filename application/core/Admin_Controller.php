<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




class Admin_Controller extends Public_Controller
{

    /**
     *
     *
     **/
    public function __construct( $nivelNecessario = 1 ) {
    	parent::__construct();		
        if (!isset($_SESSION)) {
          session_start();
        }			
		$this->loggedUserId  = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : false;	    	
		$this->loggedAdminId = isset($_SESSION['user_id']) && $_SESSION['is_admin'] !== 0 ? $_SESSION['user_id'] : false;		
        self::checkPerms( $nivelNecessario );		
     }


    /**
     *
     *
     *
     **/
    protected function checkPerms($nivelNecessario) {
		if(!$this->loggedUserId) {
			redirect('/');
			return false;	
		}		
        $nivelAtual = isset($_SESSION['is_admin']) ? $_SESSION['is_admin'] : 0;

        if( $nivelAtual >= $nivelNecessario ) {        	 
            return true;
        }
        redirect('/');
        return false;
    }

}