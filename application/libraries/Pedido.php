<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Pedidos
 */
class Pedido {
	
	private $id;
	private $cliente;
	private $obs;
	private $obsFinal;
	private $status;
	private $anexos;
	private $servicos;
	private $dataCriacao;
	private $dataAtualizacao;
    private $descricao;
    private $valorTotal;
    private $pagseguroCheckoutCode;
	
	public function	setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getId() {
		return $this->id;
	}

	public function	setCliente($cliente) {
		$this->cliente = $cliente;
		return $this;
	}

	public function getCliente() {
		return $this->cliente;
	}
	
	public function	setObs($obs) {
		$this->obs = $obs;
		return $this;
	}
	
	public function getObs() {
		return $this->obs;
	}	
	
	public function	setObsFinal($obsFinal) {
		$this->obsFinal = $obsFinal;
		return $this;
	}

	public function getObsFinal() {
		return $this->obsFinal;
	}
	
	public function	setStatus($status) {
		$this->status = $status;
		return $this;
	}

	public function getStatus() {
		return $this->status;
	}		
	
	public function setAnexos($anexos) {
		$this->anexos = $anexos;
		return $this;
	}
	
    /**
     * 
     * Retorna todos os anexos
     * 
     */
	public function getAnexos($type = false) {
		return ($type) ? $this->getAnexosByType($type) : $this->anexos;
	}
    
    /**
     * retorna os anexos finalizados
     * 
     */
    public function getAnexosByType($attachmentType = false) {
        if ($attachmentType === false) return false;
        $type = strtoupper($attachmentType);
        if(!empty($this->anexos)) {
            $anexosFiltrados = array();
            foreach ($this->anexos as $anexo) {
                if($anexo->tipo == $type) {
                    $anexosFiltrados[] = $anexo;
                }
            }
            return $anexosFiltrados;
        }
        return false;
    } 
 
	
	public function setServicos($servicos) {
		$this->servicos = $servicos;
		return $this;
	}
	
	public function getServicos() {
		return $this->servicos;
	}
	
	public function setDataCriacao($data) {
		$this->dataCriacao = $data;
		return $this;
	}
	
	public function getDataCriacao($humanFormat = false) {
		
		return $humanFormat ? $this->formatHumanDate($this->dataCriacao) : $this->dataCriacao;
	}	
	
	public function setDataAtualizacao ($data) {
		$this->dataAtualizacao = $data;
		return $this;
	}
	
	public function getDataAtualizacao() {
		return $this->dataAtualizacao;
	}
	
	private function formatHumanDate($rawDbDateTime) {
		$humanFormatedDate = date('d/m/Y', strtotime($rawDbDateTime));
		return $humanFormatedDate;
	}

    public function setDescricao ($descricao) {
        $this->descricao = $descricao;
        return $this;
    }
    
    public function getDescricao($maxChars = false) {
                        
        return ($maxChars) ? substr($this->descricao, 0, $maxChars) : $this->descricao;
    }
    
    public function setValorTotal ($valorTotal) {
        $this->valorTotal = $valorTotal;
        return $this;
    }
    
    public function getValorTotal() {
        return number_format($this->valorTotal, 2, '.', '');
    }
    
    public function setPagseguroCheckoutCode($pagseguroCheckoutCode) {
        $this->pagseguroCheckoutCode = $pagseguroCheckoutCode;
        return $this;
    }
    	
    public function getPagseguroCheckoutCode() {        
        return $this->pagseguroCheckoutCode;
    }
}
