var Viewmodel = function() {
	var self = this;
	self.totalFotos = ko.observable(0);
	self.currentPedidoId = ko.observable('');
	self.validate = function() {
		if (self.totalFotos() < 1) {
			return false;
		}
		var data = $('#form-finalizar-pedido').serialize();
		$.post(finalizarPedidoUri, data, function(result) {
			if (result.status == 'success') {
				var $row = $('[data-row-for="' + self.currentPedidoId() + '"]');
				$row.find('.badge-warning').removeClass('badge-warning').addClass('badge-success');
				$row.find('.status-text').html('Pedido Finalizado');
				$row.find('.openModalUpload').remove();
				$('#pedido-finalizado-ok').fadeIn();
				$('#uploadBtn').hide();
				setTimeout(function() {
					$('#uploadModal').modal('hide');
					$('#pedido-finalizado-ok').hide();					
					$('#uploadBtn').show();
					window.location.reload();
				}, 3000);
			}
		}, 'json');
	};

};
var viewmodel = new Viewmodel();

function removeItemFromUploadedList(itemId) {
	var photo = $('[data-image-for="' + itemId + '"]').attr('src');
	$.post(removeItemFromUploadedListUri, {
		'file' : photo
	}, function(result) {
		if (result.status === 'success') {
			$('[data-item-id="' + itemId + '"]').remove();
			$('#' + itemId).fadeOut('normal', function() {
				$(this).remove();
			});
			viewmodel.totalFotos($('[name="uploadedFiles[]"]').length);
		}
	}, 'json');
}

$(function() {
	
	ko.applyBindings(viewmodel);
	
	$('#dataInicial, #dataFinal').datepicker({
		'format' : 'dd/mm/yyyy'
	});

	$('.rm').live('click', function(e) {
		e.preventDefault();
		var rmId = $(this).attr('data-rm-id');
		removeItemFromUploadedList(rmId);
	});

	$('.openModalUpload').on('click', function(e) {
		e.preventDefault();
		var pedidoId = $(this).attr('data-pedido-id');
		viewmodel.currentPedidoId(pedidoId);
		$('.qq-upload-list li, [name="uploadedFiles[]"]').remove();
		$('#uploadModal').modal('show');
	});

	$('.producao').on('click', function() {
		var $el = $(this);
		var pedidoId = $el.attr('data-pedido-id');
		var $row = $('[data-row-for="' + pedidoId + '"]');

		$.post(enviarPedidoProducaoUri, {
			'pedido_id' : pedidoId
		}, function() {
			$row.find('.badge-info').removeClass('badge-info').addClass('badge-warning');
			$row.find('.status-text').html('Pedido em Produção');
			$el.fadeOut('slow', function() {
				$el.remove();
			});
		}, 'json');

	});

    // ajustes para paginação com query string
	$(".pagination a").each(function() {
		var g = window.location.href.slice(window.location.href.indexOf('?'));				
		var href = $(this).attr('href');
		$(this).attr('href', href + g);
	});
	
    $('body').tooltip({
      selector: "[data-toggle=tooltip]"
    });
    
 	$("a[data-toggle=popover]")
      .popover()
      .click(function(e) {
      	e.stopPropagation();
        e.preventDefault();
     });    
    
    $('.toggle-list').on('click', function(e){
    	e.preventDefault();
    	e.stopPropagation();
    	var $el = $(this);
    	var pedido = $el.attr('data-toggle-list-for');
    	var tipo = $el.attr('href').replace('#', '');    	    
    	$('.files-list').hide();
    	$('.' + tipo + ' [data-list="' + pedido + '"]').show();    	
    });
    
    $(document).on('click', function(event) {
        $('.files-list').hide();
        $("a[data-toggle=popover]").popover('hide');
    });    
 
	var errorHandler = function(event, id, fileName, reason) {
		qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
	};


	var uploader = new qq.FineUploader({
		element : $('#uploadBtn')[0],
		// debug: true,
		request : {
			endpoint : uploadFilesUri
		},
		validation : {
			allowedExtensions : ['jpeg', 'jpg', 'png', 'gif'],
			sizeLimit : 5.243e+6 // 5MB
		},
		text : {
			uploadButton : '<i class="icon-folder-open icon-white"></i> Selecionar fotos',
			dragZone : 'Arraste e solte suas fotos aqui',
			cancelButton : 'cancelar',
			retry : 'Tentar novamente',
			formatProgress : '{percent}% de {total_size}',
			failUpload : 'Falha ao enviar',
			waitingForResponse : 'Processando...'
		},
		template : '<div class="qq-uploader">' +
				   '<pre class="qq-upload-drop-area span12"><span>{dragZoneText}</span></pre>' +
				   '<div class="qq-upload-button flat-butt flat-info-butt" style="width: auto;">{uploadButtonText}</div>' +
				   '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
				   '</div>' +
				   '<div class="qq-upload-list" style="margin-top: 10px; text-align: center;">  <ul id="customUploadList" class="unstyled"></ul> </div>',
		classes : {
			//success: 'alert alert-success ',
			fail : 'alert alert-error'
		},

		callbacks : {
			onError : errorHandler,
			onComplete : function(id, name, response) {
				var info = response.uploadInfo;
				if (response.uploadInfo) {
					uniqid = Date.now();
					var uploadedFile = response.uploadInfo[0].relative_path;
					var element = $('<input type="hidden" name="uploadedFiles[]">').val(uploadedFile).attr('data-item-id', uniqid).appendTo('#ajaxUploaded');
					var thumb = $('<img />').attr({
						'src' : uploadDir + response.uploadInfo[0].relative_path,
						'data-image-for' : uniqid,
						'data-relative-path' : response.uploadInfo[0].relative_path,
					});
					var $customListEl = $('<li />').attr('id', uniqid).append(thumb);
					    $customListEl.prepend('<a href="#" class="rm" data-rm-id="' + uniqid + '">&times;</a>');
					$('#customUploadList').append($customListEl);
					viewmodel.totalFotos($('[name="uploadedFiles[]"]').length);
					$('.qq-upload-success').hide();					
				} else {
					$('#uploadBtn li').eq(id).removeClass('alert-success').addClass('alert-error');
				}
			}
		},
		failedUploadTextDisplay : {
			mode : 'custom',
			maxChars : 80,
			responseProperty : 'error',
			enableTooltip : true
		},
		deleteFile : {
			enabled : false
		}
	});
}); 