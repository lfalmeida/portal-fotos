<?php



class Formkey
{


    /**
     *
     * Guarda uma instância da classe
     *
     */
    private static $instance;


    /**
     * construtor privado; previne a criação direta do objeto
     *
     *
     */
    private function __construct() {}


    /**
     *
     *
     *
     */
    public static function singleton ()
    {
        if ( ! isset( self::$instance ) )
        {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }


	/**
	 *
	 *
	 *
	 */
	public function generateKey()
	{
		// pega o ip do usuário
		$ip = $_SERVER['REMOTE_ADDR'];

		$uniqid = uniqid(mt_rand(), true);
		//Retorna o hash
		$this->hash = md5($ip . $uniqid);

		// registra a chave para a session
		$this->registerKey();

		return $this->hash;
	}

	/**
	 *
	 *
	 *
	 */
	private function registerKey()
	{
		$_SESSION['formKey'] = $this->hash;
	}


	/**
	 *
	 *
	 *
	 */
	public function validate($key)
	{
		if( isset($_SESSION['formKey']) &&
			      $_SESSION['formKey']  == $key )
		{
			return true;
		}

		return false;

	}


    // Previne clone da instância
    public function __clone()
    {
        trigger_error('operação não permitida.', E_USER_ERROR);
    }

}