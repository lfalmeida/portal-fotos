<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Painel extends Admin_Controller
{
	/**
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct(0);
		if(!session_id()) {
			session_start();
		}
		$this->load->model(	'usuario_dao_model', 'userRepository');
        $this->load->model('pedido_dao_model', 'pedidosRepository');
	}

    /**
     *
     *
     **/
	public function index()
	{
	   $this->gerenciar_pedidos(0);
	}

    /**
     *
     * obtem os pedidos de acordo com o usuário logado
     *
     */
    private function getPedidos($options = array()) {
        $pedidos = FALSE;
        if($_SESSION['is_admin'] == 1) {
            $pedidos = $this->pedidosRepository->getAll($options);
        } else {
            $pedidos = $this->pedidosRepository->getPedidosByClienteId($this->loggedUserId, $options);
        }
        return $pedidos;
    }

    /**
     *
     *
     *
     *
     * */
    public function gerenciar_pedidos($pag = 0) {

        $this->load->library('pagination');

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url() . 'painel/gerenciar_pedidos';
        $config['total_rows'] = count($this->getPedidos());
        $config['per_page'] = 12;
        $this->pagination->initialize($config);

        $paginacao = $this->pagination->create_links();
        $limit = $config['per_page'];
        $offset = $this->uri->segment(3);
        $order = 'id DESC';
        $pedidos = $this->getPedidos(array(
            'limit' => $limit,
            'offset' => $offset,
            'order' => $order,
            //'where' => $where
                )
        );
        $this->load->vars('pedidos', $pedidos);
        $this->load->vars('paginacao', $paginacao);
        $this->load->vars('currentPage', floor(($this->uri->segment(3) / $config['per_page']) + 1));
        $this->load->view('public/painel');
    }



    /**
     *
     *
     */
    public function buscar() {

        $conditions  = array();
        $status      = $this->input->get('status');
        $pedidoId    = $this->input->get('pedido_id');
        $clienteId   = $this->input->get('cliente_id');
        $dataInicial = $this->input->get('data_inicial');
        $dataFinal   = $this->input->get('data_final');
        $statusValidos = array('A', 'E', 'P', 'F');

        if( $_SESSION['is_admin'] != 1 ) {
            $conditions[] = 'cliente_id = ' . $this->db->escape($this->loggedUserId);
        }

        if( !empty($pedidoId) && is_numeric($pedidoId) ) {
            $conditions[] = 'id = ' . $this->db->escape($pedidoId);
            $this->load->vars('filterPedidoId', $pedidoId);
        } else {
            if( !empty($status) && in_array($status, $statusValidos)){
                $conditions[] = 'status = ' . $this->db->escape($status);
                $this->load->vars('filterStatus', $status);
            }
            if( !empty($dataInicial) || !empty($dataFinal) ) {
                $conditions[] = $this->getConditionFilterDateRange($dataInicial, $dataFinal);
                $this->load->vars('filterDataInicial', $dataInicial);
                $this->load->vars('filterDataFinal', $dataFinal);
            }
            if(!empty($clienteId) && is_numeric($clienteId) ) {
                $conditions[] = 'cliente_id = ' . $this->db->escape($clienteId);
            }
        }

        $options = array();
        if(!empty($conditions)) {
            $options['where'] = implode(' AND ', $conditions);
        }

        $this->load->library('pagination');
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('painel/buscar');
        $config['total_rows'] = count($this->pedidosRepository->getAll($options));
        $config['per_page'] = 12;
        $this->pagination->initialize($config);
        $paginacao = $this->pagination->create_links();
        $options['limit']  = $config['per_page'];
        $options['offset'] = $this->uri->segment(3);
        $options['order']  = 'id DESC';
        $pedidos = $this->pedidosRepository->getAll($options);
        $this->load->vars('pedidos', $pedidos);
        $this->load->vars('paginacao', $paginacao);
        $this->load->vars('currentPage', floor(($this->uri->segment(3) / $config['per_page']) + 1));
        $this->load->view('public/painel');
    }


    /**
     *
     *
     */
    private function getConditionFilterDateRange($dataInicialRaw = '' , $dataFinalRaw = '') {

            $dataInicialRaw = (!empty($dataInicialRaw)) ? $dataInicialRaw :  date('d/m/Y', strtotime("-1 year"));
            $dataFinalRaw   = (!empty($dataFinalRaw)) ? $dataFinalRaw :  date('d/m/Y', time());

            list($diaI, $mesI, $anoI) = explode('/', $dataInicialRaw);
            list($diaF, $mesF, $anoF) = explode('/', $dataFinalRaw);
            $calcInicial = date('Y-m-d', mktime(0, 0, 0, $mesI, $diaI, $anoI));
            $calcFinal   = date('Y-m-d', mktime(0, 0, 0, $mesF, $diaF, $anoF));
            $dataInicial = $this->db->escape($calcInicial);
            $dataFinal = $this->db->escape($calcFinal);

            return 'CAST(`data_criacao` AS DATE) BETWEEN ' . $dataInicial . ' AND ' . $dataFinal;

    }

    /**
     *
     *
     */
    public function filtrar($status = false) {
        if($this->input->post('pedido_id')) {
            $pedidoId = $this->input->post('pedido_id');
            $this->getPedidoById($pedidoId);
            return true;
        }

        if($this->input->post('data_inicial') || $this->input->post('data_final')) {
            $dataInicial = $this->input->post('data_inicial');
            $dataFinal = $this->input->post('data_final');
            $this->getPedidosByDateRange($dataInicial, $dataFinal);
            return true;
        }
    }

    /**
     *
     *
     */
    public function getPedidoById($pedidoId) {
        $pedidos = $this->getPedidos(array('where' => 'id = ' . $pedidoId));
        $this->load->vars('pedidos', $pedidos);
        $this->load->view('public/painel');
    }


    /**
     *
     *
     *
     */
    public function getPedidosByDateRange($dataInicialRaw = '' , $dataFinalRaw = '') {

        $conditions = ''  ;

        if($_SESSION['is_admin'] != 1) {
            $conditions .= '`cliente_id` = ' . $this->db->escape($this->loggedUserId);
        }

        if($dataFinalRaw != '' || $dataInicialRaw != '') {
            $dataInicialRaw = $dataInicialRaw != '' ? $dataInicialRaw :  date('d/m/Y', time());
            $dataFinalRaw = $dataFinalRaw != '' ? $dataFinalRaw :  date('d/m/Y', time());

            list($diaI, $mesI, $anoI) = explode('/', $dataInicialRaw);
            list($diaF, $mesF, $anoF) = explode('/', $dataFinalRaw);
            $calcInicial = date('Y-m-d', mktime(0, 0, 0, $mesI, $diaI, $anoI));
            $calcFinal   = date('Y-m-d', mktime(0, 0, 0, $mesF, $diaF, $anoF));
            $dataInicial = $this->db->escape($calcInicial);
            $dataFinal = $this->db->escape($calcFinal);

            if(!empty($conditions)) {
                $conditions .= ' AND ';
            }
            $conditions .= 'CAST(`data_criacao` AS DATE) BETWEEN ' . $dataInicial . ' AND ' . $dataFinal;
        }

        if( $this->input->post('status') && $this->input->post('status') != '') {
            $status = $this->input->post('status');
            if(!empty($conditions)) {
                $conditions .= ' AND ';
            }
            $conditions .= ' `status` = ' . $this->db->escape($status);
        }

        $pedidos = $this->getPedidos(array('where' => $conditions ));
        $this->load->vars('pedidos', $pedidos);
        $this->load->view('public/painel');
    }



    /**
     *
     *
     *
     */
    public function finalizar_pedido() {

        $response = new stdClass();
        $response->status = 'error';

        if( ! parent::checkPerms(1)) {
            echo json_encode($response);
            return false;
        }

        if($this->input->post('pedido_id') && $this->input->post() ) {
            $pedidoId = $this->input->post('pedido_id');
            $anexos = $this->input->post('uploadedFiles');
            $obsFinal = $this->input->post('obs_final');
            if($this->pedidosRepository->finalizarPedido($pedidoId, $anexos, $obsFinal )) {
                $response->status = 'success';
                Logger::info('Pedido id:' . $pedidoId . ' finalizado');
                $this->enviarNotificacaoPedidoFinalizado($pedidoId);
            }
        }
        echo json_encode($response);
    }

    /**
     *
     *
     */
    public function enviar_pedido_producao() {
        $response = new stdClass();
        $response->status = 'error';
        if( ! parent::checkPerms(1)) {
            echo json_encode($response);
            return false;
        }

        if( $this->input->post('pedido_id') ) {
            $pedidoId = $this->input->post('pedido_id');
            if($this->pedidosRepository->updateStatus($pedidoId, 'P')) {
                $response->status = 'success';
                Logger::info('Pedido id:' . $pedidoId . ' enviado para produção');
                
                $this->enviarNotificacaoPedidoProducao($pedidoId);        
                
            }
        }
        echo json_encode($response);
    }

    /**
     * 
     * 
     */    
    public function enviarNotificacaoPedidoProducao($pedidoId) {
        $this->load->library('email');
        $this->config->load('notification');
        
        $pedido = $this->pedidosRepository->find($pedidoId);
        $cliente = $this->userRepository->find($pedido->getCliente());
 
        $this->load->vars('pedido', $pedido);
        $this->load->vars('cliente', $cliente);
  
        $message = $this->load->view('email/pedido_em_producao_cliente', '', TRUE);
   
        // enviar para o cliente
        $this->email->from($this->config->item('postmaster_address'));
        $this->email->to($cliente->getEmail());
        $this->email->subject('Seu Pedido foi para produção');
        $this->email->message($message);
  
        if($this->email->send()) {
            Logger::info('Enviado email para o cliente com detalhes sobre o pedido. Cliente: ' .
                          $cliente->getNomeCompleto() . 
                          '. Pedido id: ' . $pedido->getId() );
        }
        
        $this->email->clear();
        // enviar para o admin
        $message = $this->load->view('email/pedido_em_producao_admin', '', TRUE);
        $this->email->from($this->config->item('postmaster_address'));
        $this->email->to($_SESSION['email']);
        $this->email->subject('Pedido enviado para produção');
        $this->email->message($message);
  
        if($this->email->send()) {
            Logger::info('Enviado email para o admin com detalhes sobre o pedido em produção. Cliente: ' .
                          $cliente->getNomeCompleto() . 
                          '. Pedido id: ' . $pedido->getId() );
        }
        
    }


    /**
     * 
     * 
     */    
    public function enviarNotificacaoPedidoFinalizado($pedidoId) {
        $this->load->library('email');
        $this->config->load('notification');
        
        $pedido = $this->pedidosRepository->find($pedidoId);
        $cliente = $this->userRepository->find($pedido->getCliente());
 
        $this->load->vars('pedido', $pedido);
        $this->load->vars('cliente', $cliente);
  
        // enviar para o cliente
        $message = $this->load->view('email/pedido_finalizado', '', TRUE); 
        $this->email->from($this->config->item('postmaster_address'));
        $this->email->to($cliente->getEmail());
        $this->email->subject('Seu pedido foi finalizado!');
        $this->email->message($message);
  
        if($this->email->send()) {
            Logger::info('Enviado email de pedido finalizado. Cliente: ' .
                          $cliente->getNomeCompleto() . 
                          '. Pedido id: ' . $pedido->getId() );
        }       
    } 

    
}

