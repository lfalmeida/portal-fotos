<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Home extends Public_Controller {
	/**
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		if(!session_id()) session_start();
        $this->load->model('usuario_dao_model', 'userRepository');
	}

    /**
     *
     *
     **/
	public function index() {
 		$this->load->view('public/home');
	}

	/**
	 *
     *
	 */
	public function cadastrar_usuario() {
		if($this->input->post('submit')) {
			$usuario = new Usuario();
			$usuario->setNome($this->input->post('nome', true))
				    ->setSobrenome($this->input->post('sobrenome', true))
					->setEmail($this->input->post('email', true))
					->setSenha($this->input->post('senha', true))
					->setAdminPerm(0);
			try {
				$result = $this->userRepository->save($usuario);
				if($result) {
					$message = array(
						'status' => 'success',
						'text' => 'Cadastro realizado com sucesso.'
					);
                    
                    Logger::info("Cliente se cadastrou através do formulário público: " . $usuario->getNome());
                    
					$this->load->vars('message', (object) $message);
					$this->index();
					return true;
				}
			} catch(Exception $e) {
                                $message = array(
                                        'status' => 'warning',
                                        'text' => $e->getMessage(),
                                        'flashData' => $this->input->post(),
                                        );
                                $this->load->vars('message', (object) $message);
			}
		}
 		$this->load->view('public/usuarios/cadastrarUsuario');
	}


	/**
	 *
	 *
	 */
	public function login() {
		if($this->input->post('submit')) {
            $message = array();
            $userFoundByEmail = false;
			$email = trim($this->input->post('email', true));
			$senha = trim($this->input->post('senha', true));

            if( !empty($email) && !empty($senha) ) {
                $user = $this->userRepository->auth($email, $senha);
                if($user !== false && is_object($user)) {
                    $this->userRepository->setSessionData($user);
                    
                    $subject = ($user->isAdmin()) ? 'Administrador' : 'Cliente'; 
                    Logger::info($subject . ' fez login utilizando email e senha');
                    
                    redirect('/painel');
                    return true;
                }
                // checar se o email já está cadastrado
                $userFoundByEmail = $this->userRepository->find($email);
            }

            if($userFoundByEmail) {
                $message['status'] = 'warning';
                $message['text']   = 'Encontramos seu cadastrado mas sua senha está incorreta. <br />' .
                                     '<strong><a href="#" data-forgot-pass>Clique aqui</a> para definir uma nova senha</strong>';
                $message['email']  = $email;
            } else {
                $message['status'] = 'error';
                $message['text']   = 'Dados incorretos';
            }

            $this->load->vars('message', (object) $message);
            $this->index();
            return true;
		}
 		$this->load->view('public/home');
	}


    /**
     *
     *
     */
    public function facebook_login() {
        // limpar possíveis dados da sessão atual
        session_destroy();
        $this->load->library('facebooklib');
        if($this->facebooklib->login()) {

            Logger::info('Cliente fez login utilizando facebook');
       
            redirect('/painel');
            return true;
        }
        echo 'não logado';
    }


    /**
     *
     *
     */
     public function sair() {
         session_destroy();
         redirect('/');
     }

    /**
     *
     *
     */
    public function emailExists($email) {
        return $this->db->get_where('usuarios', array('email' => $email))->num_rows();
    }


    /**
     *
     *
     */
    public function redefinirSenha($token = false) {

        if(isset($_POST['email'])) {
             $response = array('status' => 'error', 'message' => 'error');
             $email = $this->input->post('email');
             
             if($this->emailExists($email) && $this->sendRecoveryPassToken($email) ) {
               
                Logger::info('Enviado email de recuperação de senha para: ' . $email);
                 
                $response['status']  = 'success';
                $response['message'] = 'As instruções para redefinição da sua senha foram enviadas para o seu email.';
                
             } else {
                $response['status']  = 'error';
                $response['message'] = 'Ocorreu um erro ao enviar a mensagem. Por favor, tente novamente.';
             }
             echo json_encode($response);
             return true;
        }

        if($this->input->post('senha')) {
            
            $data = array('senha' => md5($this->input->post('senha')));
            $this->db->where('id', $this->input->post('user_id'));
            $this->db->update('usuarios', $data);

            $message = array(
                'status' => 'success',
                'text' => 'Sua senha foi redefinida. Você já pode fazer login com sua nova senha.',
                'email' => $this->input->post('user_email')
            );

            Logger::info('Usuário definiu uma nova senha. user_id: ' . $this->input->post('user_id') );

            $this->load->vars('message', (object) $message);
            $this->index();
            return true;
        }

        if( $token !== false ) {
            $user = $this->getUserByRecoveryPassToken($token);
            $this->load->vars('recoverPassUser', $user);
            $this->load->view('public/usuarios/resetPass');
            return true;
        }
    }


    /**
     *
     *
     */
    private function getUserByRecoveryPassToken($compositToken) {
        list($token, $mailHash) = explode(':', $compositToken);
        $request = $this->db->get_where('forgot_pass', array('token' => $token))->row();
        $user = $this->userRepository->find($request->email);
        return ($user != false ) ? $user : false;
    }


    /**
     *
     *
     */
    public function sendRecoveryPassToken($email) {
               
        $token   = $this->createRecoveyPassToken($email);
        $message = '<p>Foi solicitada uma recuperação de senha para seu cadastro. </p>
                    <p>Para criar uma nova senha para sua conta, favor clicar no link abaixo:</p>' .
                    '<p><a href="' . site_url('home/redefinirSenha/' . $token ) . '">redifinir minha senha</a></p>' .
                    '<p>Caso não tenha sido você quem solicitou este e-mail, basta ignorá-lo.</p>';

        $this->load->library('email');
        $this->email->from('fernando@ebercom.com.br', 'Portal');
        $this->email->to($email);
        $this->email->subject('Redefinir senha');
        $this->email->message($message);

        return $this->email->send();
    }


    /**
     *
     *
     */
    private function createRecoveyPassToken($email) {

        $randHash = md5(rand());
        $mailHash = md5($email);
        $token = $randHash . ':' . $mailHash;
        $this->db->delete('forgot_pass', array('email' => $email));
        $this->db->insert('forgot_pass', array('email' => $email, 'token' => $randHash));

        return $token;
    }




    /**
     *
     *
     */
    public function download_zip($tipo = false, $pedidoId = false) {
       if(!$tipo || !$pedidoId) redirect('/');
       $tipo = strtoupper($tipo);
       $tiposValidos = array('F', 'O');
       if(!in_array($tipo, $tiposValidos)) redirect('/');

       $this->load->model('pedido_dao_model', 'pedidoRepository');
       $this->load->library('zip');
       $pedido = $this->pedidoRepository->find($pedidoId);

       $anexos = $pedido->getAnexos($tipo);

        if(!empty($anexos)){
           $zipFilename = 'arquivos_' . $tipo . '_' .$pedido->getId() . '.zip' ;
           foreach ($anexos as $arquivo) {
               $file = 'files/docs/' . $arquivo->anexo;
               $this->zip->read_file($file);
           }
           $this->zip->download($zipFilename);
        } else {
            redirect('/');
        }
    }


}
