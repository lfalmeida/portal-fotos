<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Servico_dao_model extends CI_Model 
{

	const     TABLE_NAME = 'servicos',
				   	  PK = 'id'; // Primary Key
	/**
     *
     *
     *
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Cria ou altera um registro, caso o
	 * objeto tenha ou não um atributo 'id'
	 *
	 */
	public function save( $servico )
	{
		$data = array(
					'nome' => $servico->getNome(),
					'descricao' => $servico->getDescricao(),
					'preco' => $servico->getPreco($toDB = true),
				);

		if( $servico->getId()) {
			// alterar registro
			$data[self::PK] = $servico->getId();
			return self::update($data);
		}
		return self::insert($data);
	}


	/**
	 * Insere um registro no banco
	 *
	 *
	 */
	private function insert($data)
	{
		if( $this->db->insert(self::TABLE_NAME, $data) )
		{
			return $this->db->insert_id();
		}
		return false;
	}


	/**
	 * Altera um registro do banco
	 *
	 *
	 */
	private function update($data)
	{
		$this->db->where(self::PK, $data[self::PK] );

		if( $this->db->update(self::TABLE_NAME, $data) )
		{
			return true;
		}
		return false;
	}


	/**
	 * Remove um registro do BD
	 *
	 *
	 */
	public function delete( $identifier )
	{
		return $this->db->delete(self::TABLE_NAME, array(self::PK => $identifier ) );
	}


	/**
	 * Obtem registros do banco de dados
	 * de acordo com as opções fornecidas
	 *
	 */
	public function getAll(array $options = array())
	{
        if( isset( $options['order'] ) AND null != $options['order'] ) {
            $this->db->order_by($options['order']);
        }

        if( isset( $options['where'] ) AND null != $options['where'] ) {
            $this->db->where($options['where']);
        }

        if( isset($options['limit']) AND isset($options['offset']) ) {
            $this->db->limit($options['limit'], $options['offset']);
        }

        $result = $this->db->get(self::TABLE_NAME)->result();

        return $this->build($result);
	}


	/**
	 * Obtem um registro do banco de acordo com
	 *  o identificador informado
	 *
	 *
	 *
	 */
	public function find($identifier,  $returnObject = true)
	{
		// chave primária
		if(is_numeric($identifier))
		{
			$this->db->where(self::PK, $identifier);
			$result = $this->db->get(self::TABLE_NAME)->result();
			return $this->build($result, $returnObject);
		}

        return false;
	}

    /**
     * 
     * 
     */
    public function getServicosByPedidoId($pedidoId) {
            
        $this->db->where('pedido_id', $pedidoId);
        $result = $this->db->get('servicos_pedido')->result();

        $servicos = array();
        foreach ($result as $item) {
            $servicos[] = $this->find($item->servico_id);
        }

        return $servicos;
                
    }
    
    
	/**
	 *
	 * Constrói e retorna objetos ou array de objetos da tabela
	 * Recebe um objeto ou array de objetos
	 * resultante de uma consulta ao BD.
	 * Pode retornar um objeto ou um array de objetos.
	 *
	 */
	public function build ( $data, $returnObject = false )
	{
		if( is_array($data) ) {

			$servicos = array();

			foreach ($data as $row)
			{
				$servico = new Servico();
				$servico->setId($row->id)
						->setNome($row->nome)
						->setDescricao($row->descricao)
						->setPreco($row->preco);

				$servicos[] = $servico;
			}

			if( count($servicos) == 1 &&  $returnObject != false )
			{
				return $servicos['0'];
			}
			return empty($servicos) ? false : $servicos;
		}
		return false;
	}

     /**
      *
      *
      */
      public function excluir($servicoId) {
          if(!is_numeric($servicoId)) {
              throw new Exception('Para excluir um serviço é preciso informar o id', 1);
              return false;
          }

          $this->db->where('id', $servicoId);
          return $this->db->delete('servicos');
      }

}
