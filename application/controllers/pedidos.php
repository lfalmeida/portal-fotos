<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Pedidos extends Admin_Controller
{

	/**
	 *
	 *
	 *
	 */
	public function __construct() {
		parent::__construct($nivelNecessario = 0);
		$this->load->model('servico_dao_model', 'servicoRepository');
		$this->load->model('pedido_dao_model', 'pedidoRepository');
        $this->load->model('usuario_dao_model', 'usuarioRepository');
	}


    /**
     * redirecionando o método padrão do controller
     *
     *
     **/
	public function index() {
        $this->gerenciar(0);
	}


	/**
	 *
	 */
	public function novo() {
		$servicosDisponiveis = $this->servicoRepository->getAll();
		$this->load->vars('servicosDisponiveis', $servicosDisponiveis);

 		if($this->input->post('uploadedFiles')) {
			$pedido = new Pedido();

			$pedido->setCliente($this->loggedUserId)
				   ->setObs($this->input->post('obs', true))
				   ->setServicos($this->input->post('servicos'))
				   ->setAnexos($this->input->post('uploadedFiles'))
				   ->setStatus('A'); // aguardando pagamento

            try {
                $pedidoId = $this->pedidoRepository->save($pedido);
                if($pedidoId != false) {

                    Logger::info('Novo pedido efetuado. id:' . $pedidoId);

                    $this->sendNotifications($pedidoId);    

                    $this->processPayment($pedidoId);
                }
            } catch (Exception $e) {
                // TODO mostrar essa mensagem mais adequadamente
                echo $e->getMessage();
                return FALSE;
            }
 		}
 		$this->load->view('public/pedidos/novo');
	}


    /**
     * Recebe o id do pedido e direciona para uma nova solicitação de 
     * pagamento
     */
    public function restart($pedidoId) {
        Logger::info('Cliente reiniciou o pagamento através do botão do painel. Id do pedido: ' . $pedidoId);
        $this->processPayment($pedidoId);
    }


    /**
     *
     *
     */
    private function processPayment($pedidoId) {
        $this->processPaymentPagSeguro($pedidoId);
    }



    public function processPaymentPagSeguro($pedidoId) {
        $this->load->library('PagSeguroLibrary/PagSeguroLibrary');

        $pedido = $this->pedidoRepository->find($pedidoId);
        $cliente = $this->usuarioRepository->find($pedido->getCliente());

        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");
        
        // até 100 caracteres
        // $descricao = $pedido->getDescricao(100); 
        $descricao = "Tratamento de fotos";
        
        $paymentRequest->addItem($pedido->getId(), $descricao, 1, $pedido->getValorTotal());
        
        $paymentRequest->setReference($pedido->getId());
        $paymentRequest->setSender(
            $cliente->getNomeCompleto(),
            $cliente->getEmail()
        );

        $redirectUrl = site_url('retorno');
        $paymentRequest->setRedirectUrl($redirectUrl);

        $notificationUrl = site_url('retorno/notificacoes');
        $paymentRequest->addParameter('notificationURL',  $notificationUrl);

        try {
            $credentials = new PagSeguroAccountCredentials(
                $this->config->item('pagseguroAccount'),
                $this->config->item('pagseguroToken')
            );

            $onlyCheckoutCode = true;
            $code = $paymentRequest->register($credentials, $onlyCheckoutCode);

            if($code) {

                Logger::info('Requisição de pagamento criada com sucesso. Redirecionar para o pagseguro com o código: ' . $code);

                $this->pedidoRepository->savePagseguroCheckoutCode($pedido->getId(), $code);
                redirect('https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $code);

                return true;
            }
        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }




    /**
     * Método usado para salvar as fotos selecinadas via
     * javascript
     *
     */
	public function save_facebook_photo() {
		$result = new stdClass();
		$result->status = 'error';

		if($this->input->post('url')) {
			$url = $this->input->post('url');
			$this->load->model('upload_model');
			$result->status = 'success';
			$result->data = $this->upload_model->saveFromUrl($url);
			// caminho formatado para ser salvo no banco
			list($file, $month, $year, $type, $dir) = array_reverse(explode('/', $result->data));
			$result->relative_path = $year . '/' . $month . '/' . $file;
		}
		echo json_encode($result);
	}


    /**
     *
     * Remove a foto
     *
     */
	public function rmPhoto() {
		$response = new stdClass();
		$response->status = 'error';
		if($this->input->post('file')) {
			list($file, $month, $year, $type, $dir) = array_reverse(explode('/', $this->input->post('file')));

			$file = $dir . '/' . $type . '/' . $year . '/' . $month . '/' . $file;

			if(is_file($file) && unlink($file) ) {
				$response->status = 'success';
			}
		}
		echo json_encode($response);
	}


    /**
     * 
     * Envia os emails quando um pedido é realizado
     * 
     */
    private function sendNotifications($pedidoId) {
            
        $this->load->library('email');
        $this->config->load('notification');
        
        $pedido = $this->pedidoRepository->find($pedidoId);
        $cliente = $this->usuarioRepository->find($pedido->getCliente());
 
        $this->load->vars('pedido', $pedido);
        $this->load->vars('cliente', $cliente);
  
        $message = $this->load->view('email/novo_pedido', '', TRUE);
   
        // enviar para o cliente
        $this->email->from($this->config->item('postmaster_address'));
        $this->email->to($cliente->getEmail());
        $this->email->subject('Recebemos seu pedido');
        $this->email->message($message);
  
        if($this->email->send()) {
            Logger::info('Enviado email para o cliente com detalhes sobre o pedido. Cliente: ' .
                          $cliente->getNomeCompleto() . 
                          '. Pedido id: ' . $pedido->getId() );
        }
        
    }



}
