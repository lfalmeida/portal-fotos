$(function() {

    var errorHandler = function(event, id, fileName, reason) {
        qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
    };

    var uploader = new qq.FineUploader({
        element: $('#uploadBtn')[0],
       // debug: true,
        request: {
            endpoint: "/ci/upload"
        },
	      validation: {
	        allowedExtensions: ['jpeg', 'jpg', 'png', 'gif'],
	        sizeLimit: 5.243e+6 // 5MB
	      },        
        text: {
            uploadButton: '<i class="icon-folder-open icon-white"></i> Selecionar fotos do meu computador',
            dragZone: 'Arraste e solte suas fotos aqui',
            cancelButton: 'cancelar',
            retry: 'Tentar novamente',
            formatProgress: '{percent}% de {total_size}',
            failUpload: 'Falha ao enviar',
            waitingForResponse: 'Processando...'
        },
        template: '<div class="qq-uploader">' +
                  '<pre class="qq-upload-drop-area span12"><span>{dragZoneText}</span></pre>' +
                  '<div class="qq-upload-button flat-butt flat-info-butt" style="width: auto;">{uploadButtonText}</div>' +
                  '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
                  '</div>' +                  
                  '<ul class="qq-upload-list" style="margin-top: 10px; text-align: center;"></ul>'
                  ,
        classes: {
            //success: 'alert alert-success ',
            fail: 'alert alert-error'
        },

        callbacks: {
            onError: errorHandler,
            onComplete: function(id, name, response){
            	
            	console.log(id);
            	console.log(name);
            	console.log(response);
            	
                var info = response.uploadInfo;

                if(response.uploadInfo) {
                	uniqid = Date.now();
                	
                    var uploadedFile = response.uploadInfo[0].relative_path;
                    var element = $('<input type="hidden" name="uploadedFiles[]">').val(uploadedFile)
                    															   .attr('data-item-id', uniqid)
                                                                                   .appendTo('#ajaxUploaded');
                                                                                       
                     var thumb = $('<img />').attr({
                     	'src': '/ci/files/docs/' + response.uploadInfo[0].relative_path,
                     	'data-image-for': uniqid,
                     	'data-relative-path': response.uploadInfo[0].relative_path,
                     });
                     
                     $('#uploadBtn li').eq(id).attr('id', uniqid).append(thumb);
                     
                 	 $('#'+uniqid).prepend('<a href="#" class="rm" data-rm-id="' + uniqid + '">&times;</a>');
                     
                     viewmodel.totalFotos($('[name="uploadedFiles[]"]').length); 
                                        
                } else {
                    $('#uploadBtn li').eq(id).removeClass('alert-success').addClass('alert-error');
                }
            }
        },
        failedUploadTextDisplay: {
            mode: 'custom',
            maxChars: 80,
            responseProperty: 'error',
            enableTooltip: true
        },
        deleteFile: {
            enabled: false
        }
    });


});
