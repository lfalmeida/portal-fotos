<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * class Pessoa
 *
 */
abstract class Pessoa {

	/**
	 *
	 * @access protected
	 */
	protected $id;

	/**
	 *
	 * @access protected
	 */
	protected $nome;

	/**
	 *
	 * @access protected
	 */
	protected $email;

	/**
	 *
	 * @access protected
	 */
	protected $status;

	/**
	 *
	 *
	 * @return
	 * @access public
	 */
	public function setId($id) {
		$this -> id = $id;
	}// end of member function setId

	/**
	 *
	 *
	 * @return int
	 * @access public
	 */
	public function getId() {
		return $this -> id;
	}// end ofmember function getId

	/**
	 *
	 *
	 * @return
	 * @access public
	 */
	public function setNome($nome) {
		$this -> nome = $nome;
	}// end of member function setNome

	/**
	 *
	 *
	 * @return varchar
	 * @access public
	 */
	public function getNome() {
		return $this -> nome;
	}// end of member function getNome

	/**
	 * Método setter para a propriedade $email
	 *  @param string $email
	 *
	 **/
	public function setEmail($email) {

		if (!empty($email) AND filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {

			$this -> email = $email;

		} else {

			#       throw new Exception('O email informado é inválido');
			return false;

		}

	}

	/**
	 * Método getter para a propriedade $email
	 *  @return string
	 *
	 **/
	public function getEmail() {
		return $this -> email;
	}

	/**
	 *
	 *
	 * @return
	 * @access public
	 */
	public function setStatus($status) {
		$this -> status = $status;
	}// end of member function setStatus

	/**
	 *
	 *
	 * @return bool
	 * @access public
	 */
	public function getStatus() {
		return $this -> status;
	} // end of member function getStatus

} // end pessoa
