<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Upload_model extends CI_Model
{

    public function __construct()
    {
    	$this->load->library('image_lib');
        parent::__construct();
    }


    /**
     *
     *
     *
     *
     **/
    function upload($uploadDir = 'docs')
    {

        $files = array();

        $config['upload_path']   = $this->getDir($uploadDir);
        $config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|pdf|xls|xlsx';
        $config['max_width']     = '5200';
        $config['max_height']    = '5200';
        $config['encrypt_name']  = TRUE;

        $this->load->library('upload', $config);

        $errors = FALSE;

        foreach($_FILES as $key => $value)
        {
            if( ! empty($value['name']))
            {
                if( ! $this->upload->do_upload($key))
                {
                    $data['upload_message'] = $this->upload->display_errors('','');

                    throw new Exception($this->upload->display_errors('',''));

                    $this->load->vars($data);

                    $errors = TRUE;
                }
                else
                {
                    $upload_data         = $this->upload->data();
                    $upload_data['hash'] = md5( mt_rand(0, time() ) ); // adicionar um hash para este arquivo
                    $upload_data['relative_path'] = $this->getDir() . strtolower($upload_data['file_name']);
                    $files[]             = $upload_data;
                }
            }
        }


        if($errors)
        {
            foreach($files as $key => $file)
            {
                @unlink($file['full_path']);
            }
        }
        elseif(empty($files) AND empty($data['upload_message']))
        {
            $this->lang->load('upload');
            $data['upload_message'] =  $this->lang->line('upload_no_file_selected');
            $this->load->vars($data);
        }
        else
        {
            return $files;
        }


    }




    /**
     *
     * @param array  $file
     * @param int
     *
     **/
    function resize($file, $width, $height, $dir, $manter_orig = false)
    {

        if( ! is_array($file) ) return false;

        $fileName = $file['hash'] . strtolower($file['file_ext']);
        $fileFullPath = $dir . $fileName;


       // Se a imagem não for muito grande não redimensionar
       if ( $file['image_width'] < $width  )
       {
            // adiciona watermark
            $this->addWaterMark($file['full_path']);

            copy($file['full_path'], $fileFullPath);

            if( false == $manter_orig AND file_exists($file['full_path']))
            {
                  @unlink($file['full_path']);
            }

       	   return $fileName;
       }

              // adiciona watermark
        $this->addWaterMark($file['full_path']);

        $this->image_lib->clear();
         $config['image_library'] = 'gd2';
          $config['source_image'] = $file['full_path'];
             $config['new_image'] = $fileFullPath;
        $config['maintain_ratio'] = TRUE;
                 $config['width'] = $width;
                $config['height'] = $height;

        $this->image_lib->initialize($config);

        $this->image_lib->resize();
        unset($config);



        // deletar o arquivo temporário
        if( false == $manter_orig AND file_exists($file['full_path']))
        {
              @unlink($file['full_path']);
        }


        return $fileName;

    }



    /**
     *
     * Retorna o diretório em que as imagens devem ser salvas
     * Cria estes diretórios se necessário
     *
     */
    function getDir($tipo = false, $tamanho = false)
    {
 
        $images_path = 'files/';

        if( $tipo == false && $tamanho == false)
        {
            return date('Y/m/');
        }
        else
        {
	        $tamanhoDir = ($tamanho != false) ? "/$tamanho" : '';
	        $path = $images_path . $tipo . $tamanhoDir . '/';


            if( !is_dir( $path . date('Y/m')) )
            {
                  mkdir( $path . date('Y/m'), 0777, true);
            }


            if( file_exists( $path . date('Y/m')) )
            {
                 return $path . date('Y/m/') ;
            }
            else
            {
                return false;
            }
        }
    }

	public function saveFromUrl($url) {
		
		$ext = substr(strrchr($url,'.'),1); 	
		$source = file_get_contents($url);
		
		$newFilename = $this->getDir('docs') . md5(time()) . '_fb.' . $ext; 
		
		if(file_put_contents($newFilename, $source)) {
			return $newFilename;
		}
		

	}

    function addWaterMark($source)
    {
    	$this->image_lib->clear();

		$config['source_image']     = $source;
		$config['wm_type']          = 'overlay';
		$config['wm_opacity']       = '90';
		$config['wm_vrt_alignment'] = 'middle';
		$config['wm_hor_alignment'] = 'center';
		$config['wm_padding']       = '0';
        $config['wm_overlay_path']  = 'assets/images/wm.png';

		$this->image_lib->initialize($config);

		$this->image_lib->watermark();
    }




}
