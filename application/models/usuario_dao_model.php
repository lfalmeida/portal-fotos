<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Usuario_dao_model extends CI_Model  
{

	const     TABLE_NAME = 'usuarios',
				   	  PK = 'id'; // Primary Key
	/**
     *
     *
     *
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Cria ou altera um registro, caso o
	 * objeto tenha ou não um atributo 'id'
	 *
	 */
	public function save( $usuario )
	{
		$data = array(
					'nome' => $usuario->getNome(),
					'sobrenome' => $usuario->getSobrenome(),
					'email' => $usuario->getEmail(),
					'senha' => $usuario->getSenha(),
					'facebook_id' => $usuario->getFacebookId(),
					'admin' => $usuario->isAdmin(),
				);

		if( $usuario->getId()) // alterar registro
		{
			$data[self::PK] = $usuario->getId();
			return self::update($data);
		}
		return self::insert($data);
	}


	/**
	 * Insere um registro no banco
	 *
	 *
	 */
	private function insert($data)
	{
		$userExist = $this->find($data['email'],true);

		if( $userExist != false && is_object($userExist) ) {
            // $data[self::PK] = $userExist->getId();
            // $data['facebook_id'] = $userExist->getFacebookId();
            // return self::update($data);
		    $message = 'O email ' . $data['email'] . ' já está cadastrado no sitetema.';
		    throw new Exception($message, 1);
			return false;
		}

		$data['senha'] = md5($data['senha']);

		if( $this->db->insert(self::TABLE_NAME, $data) ) {
			return $this->db->insert_id();
		}
		return false;
	}


	/**
	 * Altera um registro do banco
	 *
	 *
	 */
	private function update($data)
	{
		$this->db->where(self::PK, $data[self::PK] );

        // se a senha veio vazia não alterar
        if( isset($data['senha']) && empty($data['senha']) ) {
            unset($data['senha']);
        } else {
           // criptografar a nova senha
           $data['senha']  = md5($data['senha']);
        }

		if( $this->db->update(self::TABLE_NAME, $data) ){
			return true;
		}
		return false;
	}



	/**
	 * Remove um registro do BD
	 *
	 *
	 */
	public function delete( $identifier )
	{
		return $this->db->delete(self::TABLE_NAME, array(self::PK => $identifier ) );
	}


	/**
	 *
	 * Insere um novo usuário a partir dos dados do facebook
	 *
	 */
	public function saveUserFromFacebook($user)
	{	  
		if(!empty($user) && isset($user['email'])) {
			$userExists = $this->find($user['email']);

				$usuario = new Usuario();
				$usuario->setNome($user['first_name'])
						->setSobrenome($user['last_name'])
						->setEmail($user['email'])
						->setFacebookId($user['id'])
                        ->setAdminPerm(0); 

				if($userExists !== false )
				{
					$usuario->setId($userExists->getId());
				}
				$this->save($usuario);
		}
	}

	/**
	 * Obtem registros do banco de dados
	 * de acordo com as opções fornecidas
	 *
	 */
	public function getAll(array $options = array())
	{
        if( isset( $options['order'] ) AND null != $options['order'] ) {
            $this->db->order_by($options['order']);
        }

        if( isset( $options['where'] ) AND null != $options['where'] ) {
            $this->db->where($options['where']);
        }

        if( isset($options['limit']) AND isset($options['offset']) ) {
            $this->db->limit($options['limit'], $options['offset']);
        }

        $result = $this->db->get(self::TABLE_NAME)->result();

        return $this->build($result);
	}


	/**
	 * Obtem um registro do banco de acordo com
	 *  o identificador informado
	 *
	 *
	 *
	 */
	public function find($identifier,  $returnObject = true)
	{
		// chave primária
		if(is_numeric($identifier))
		{
			$this->db->where(self::PK, $identifier);
			$result = $this->db->get(self::TABLE_NAME)->result();
			return $this->build($result, $returnObject);
		}

		// busca um usuario pelo endereco de email
		if( strpos($identifier, '@') !== false ) {
			$this->db->where('email', $identifier);
			$result = $this->db->get(self::TABLE_NAME)->result();
			return $this->build($result, $returnObject);
		}

        return false;
	}


	/**
	 *
	 * Constrói e retorna objetos ou array de objetos da tabela
	 * Recebe um objeto ou array de objetos
	 * resultante de uma consulta ao BD.
	 * Pode retornar um objeto ou um array de objetos.
	 *
	 */
	public function build ( $data, $returnObject = false )
	{
		if( is_array($data) ) {
			$usuarios = array();
			foreach ($data as $row)
			{

				$usuario = new Usuario();
				$usuario->setId($row->id)
						->setNome($row->nome)
					    ->setSobrenome($row->sobrenome)
						->setEmail($row->email)
						->setSenha($row->senha)
						->setFacebookId($row->facebook_id)
						->setAdminPerm($row->admin);

				$usuarios[] = $usuario;
			}

			if( count($usuarios) == 1 &&  $returnObject != false )
			{
				return $usuarios['0'];
			}
			return empty($usuarios) ? false : $usuarios;
		}
		return false;
	}

	/**
	 *
	 */
	public function auth($email, $senha) {
		$where = 'email = ' . $this->db->escape($email) .
		         ' AND senha = ' . $this->db->escape(md5($senha));

		$user = $this->getAll(array('where' => $where));

		return (!empty($user)) ? $user['0'] : false ;
	}

	/**
	 *
	 *
	 */
	 public function setSessionData($userData)
	 {
	 	if(!session_id()) {
			session_start();
		}

	 	// Array $me do facebook
	 	if(is_array($userData)) {
			$userData = $this->find($userData['email'], true);
	 	}

		// Objeto Usuário
		$_SESSION['user_id'] = $userData->getId();
		$_SESSION['facebook_id'] = $userData->getFacebookId();
		$_SESSION['name'] = $userData->getNome();
		$_SESSION['is_admin'] = $userData->isAdmin();
        $_SESSION['email'] = $userData->getEmail();
        
	 }


     /**
      *
      *
      */
      public function excluir($usuarioId) {
          if(!is_numeric($usuarioId)) {
              throw new Exception('Para excluir um usuário é preciso informar o id', 1);
              return false;
          }
          // Um admin não pode se excluir
          if( $_SESSION['is_admin'] == 1 && $usuarioId == $_SESSION['user_id']) {
              throw new Exception('Desculpe, no momento você não pode excluir a sua própria conta.', 1);
              return false;
          }

          $this->db->where('id', $usuarioId);
          return $this->db->delete('usuarios');
      }

}
