<?php


// For overriding Native class to  convert filenames to lowercase.
class MY_Upload extends CI_Upload
{

    function _prep_filename($filename)
    {
        $filename = strtolower($filename);
        return parent::_prep_filename($filename);
    }

    function get_extension($filename)
    {
        $filename = strtolower($filename);
        return parent::get_extension($filename);
    }


}
