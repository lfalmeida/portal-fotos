        window.fbAsyncInit = function() {
            FB.init({
                appId : fbAppId,
                status : true, // check login status
                cookie : true, // enable cookies to allow the server to access the session
                xfbml : true // parse XFBML
            });

            FB.Event.subscribe('auth.authResponseChange', function(response) {
                console.log(response);
            });
        };

        // Load the SDK asynchronously
        (function(d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));        
        
		$(function(){

      		$('#fb-login').on('click', function() {
        		FB.getLoginStatus(function(response){
            		if(response.status === 'connected') {
            		    window.location.href = fbLoginUrl;
            		} else {
            		    FB.login(function(response) {
            		        if(response.authResponse) {
            		           window.location.href = fbLoginUrl;
            		        } 
            		    },{scope: 'email,user_photos'});
            		}
        		});
    		});

            $('[data-forgot-pass]').on('click', function(e){
                e.preventDefault();
                $('#login').hide();
                $('#resetPass').fadeIn();
            });

            $('#show-login').on('click', function(e){
                e.preventDefault();
                $('#resetPass').hide();
                $('#login').fadeIn();
            });

            
            $('#resetPasswd').on('submit', function(e) {
                e.preventDefault();
                var $form = $(this);
                    email = $('#email_recover').val();

                if(email.length < 5 ) return false;

                $.post(redefinirSenhaUri, {'email': email}, function(result){
                    if(result.status !== 'undefined') {
                        $('#message').removeClass('alert alert-success alert-error')
                                     .addClass('alert alert-' + result.status )
                                     .html(result.message)
                                     .fadeIn();

                        if(result.status == 'success') {
                            $('#continuar').hide();
                        }

                    }
                }, 'json');
            });

            $('#email_recover').on('focus', function(){
                $('#message').hide();
            });
        });