<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 */
class Upload extends Public_Controller {

    /**
     *
     *
     *
     *
     **/
    public function __construct()
    {
        parent::__construct();
     }

    public function index()
    {
        $this->load->model('upload_model');
        $response = array();

        try {


            $response['uploadInfo'] = $this->upload_model->upload();
            $response['success'] = true;

        } catch (Exception $e) {

            $response['error'] = $e->getMessage();

        }

        echo json_encode($response);
    }

}
