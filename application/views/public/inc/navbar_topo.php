    <div class="navbar navbar-static-top navbar-blue">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo site_url('/painel'); ?>">Portal de Tratamento de fotos</a>
          
		<?php if($_SESSION['is_admin'] == 1):?>
            <ul class="nav" style="margin-top:5px;margin-left:40px;">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuários <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('usuarios/cadastrar')?>"><i class="icon-plus"></i> Cadastrar usuário</a></li>
                  <li><a href="<?php echo site_url('usuarios/gerenciar')?>"><i class="icon-list"></i> Gerenciar usuários</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Serviços <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('servicos/cadastrar')?>"><i class="icon-plus"></i> Cadastrar Serviço</a></li>
                  <li><a href="<?php echo site_url('servicos/gerenciar')?>"><i class="icon-list"></i> Gerenciar Serviços</a></li>
                </ul>
              </li>                            
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Clientes <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('clientes/cadastrar')?>"><i class="icon-plus"></i> Cadastrar clientes</a></li>
                  <li><a href="<?php echo site_url('clientes/gerenciar')?>"><i class="icon-list"></i> Gerenciar clientes</a></li>
                </ul>
              </li>
            </ul>
         <?php endif; ?> 
          
		  <div class=" pull-right">
			<ul class="nav" style="margin-top:5px;margin-left:40px;">
			  <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['name']; ?> <b class="caret"></b></a>
				<ul class="dropdown-menu">
				   <li><a href="<?php echo site_url('home/sair')?>"><i class="icon-off"></i> Sair</a></li>
				</ul>
			  </li>
			</ul>
		  </div>
          
 
          
           
        </div>
      </div>
    </div>
    
<div class="container-fluid">

 

 
 
