<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * 
 */
class Servico  {
	
	
	private $id;
	private $nome;
	private $descricao;
	private $preco;
	
	public function setId($id) 
	{
		$this->id = $id;
		return $this;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setNome($nome) 
	{
		$this->nome = $nome;
		return $this;
	}
	
	public function getNome()
	{
		return $this->nome;
	}
	
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
		return $this;
	}
	
	public function getDescricao()
	{
		return $this->descricao;
	}
	
	public function setPreco($preco)
	{
		$this->preco = $preco;
		return $this;
	}
	
	public function getPreco($toDB = false)
	{
		return ($toDB) ? str_replace(',', '.', $this->preco): $this->preco;
	}
	
	public function getPrecoFormatado()
	{
		return number_format($this->preco, 2, ',', '.');
	}	
	
}
