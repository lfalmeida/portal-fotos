var Viewmodel = function() {
	var self = this;
	self.totalFotos = ko.observable(0);
	self.custoServicosSelecionados = ko.observable(0);
	self.message = ko.observable('');
	self.me = ko.observable('');

	self.valorTotalPedido = ko.computed(function() {
		var total = self.totalFotos() * self.custoServicosSelecionados();
		return total.toFixed(2).replace('.', ',');
	});

	self.facebookPhotos = ko.observableArray([]);

	self.addFacebookPhoto = function(data) {
		var url = data.images[0].source;
		var $item = $('#' + data.id);
		$item.removeClass('processed');
		$item.addClass('processing');
		saveFacebookPhoto(url).done(function(result) {
			if (result.status === 'success') {
				addItemToUploadedList(result.data, result.relative_path);
				$item.removeClass('processing');
				$item.addClass('processed');
			}
		});
	};

	self.validate = function() {
		if (self.totalFotos() < 1 || self.custoServicosSelecionados() === 0) {
			self.message({
				'type' : 'error',
				'text' : 'Selecione ao menos uma foto e um serviço a ser executado.'
			});
			setTimeout(function() {
				$('.alert.alert-danger').fadeOut('slow');
			}, 5000);
			return false;
		}
		return true;
	};

};

var viewmodel = new Viewmodel();
//viewmodel.facebookPhotos().data = [];

window.fbAsyncInit = function() {
	FB.init({
		appId : facebookAppId,
		status : true, // check login status
		cookie : true, // enable cookies to allow the server to access the session
		xfbml : true // parse XFBML
	});

	FB.Event.subscribe('auth.authResponseChange', function(response) {
		if (response.status === 'connected') {
			fetchFacebookUserData();			
		} else if (response.status === 'not_authorized') {
			FB.login({
				scope : 'email,user_photos'
			});
		} else {
			FB.login({
				scope : 'email,user_photos'
			});
		}
	});
};

// Load the SDK asynchronously
(function(d) {
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement('script');
		js.id = id;
		js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
}(document));


function fetchFacebookUserData() {
	
	
	FB.api('/me/albums', function(response) {
		$.each(response.data, function(key, album){
			FB.api('/' + album.id + '/photos', function(fotos) {
				if(fotos.data.length > 0 ) {
					$.each(fotos.data, function(key, foto){
						console.log(foto);
						viewmodel.facebookPhotos.push(foto);		
					});
				 
				}
				
			});			
		});
	});
	
	
	FB.api('/me/photos', function(response) {
		// console.log('---');
		// console.log(response);
		// console.log('---');
		//viewmodel.facebookPhotos(response.data);
	});
	
	
	FB.api('/me', function(response) {
		viewmodel.me(response);
	});
	
}


function loginFacebook(callback) {
	FB.login(function(response) {
		if (callback && typeof(callback) === "function") {
		    callback();
		}
	}, {
		scope : 'email,user_photos'
	});
}

function logoutFacebook(callback) {
	FB.logout(function(response) {
		if (callback && typeof(callback) === "function") {
		    callback();
		}
	});
}

function saveFacebookPhoto(url) {
	return $.post(saveFacebookPhotoUri, {
		'url' : url
	}, function() {
	}, 'json');
}

function addItemToUploadedList(url, relativePath) {
	uniqid = Date.now();
	var src = urlRoot + url;
	var $list = $('.fb-upload-list'), $li = $('<li>').attr('id', uniqid), $img = $('<img>').attr({
		'src' : src,
		'data-relative-path' : url,
		'data-image-for' : uniqid
	}), $inputHolder = $('#ajaxUploaded'), $input = $('<input type="hidden" name="uploadedFiles[]">').val(relativePath).attr('data-item-id', uniqid);

	$li.append($img).prepend('<a href="#" class="rm" data-rm-id="' + uniqid + '">&times;</a>');
	$list.prepend($li);
	$inputHolder.append($input);
	viewmodel.totalFotos($('[name="uploadedFiles[]"]').length);
}

function removeItemFromUploadedList(itemId) {
	var photo = $('[data-image-for="' + itemId + '"]').attr('src');
	$.post(removeItemFromUploadedListUri, {
		'file' : photo
	}, function(result) {
		if (result.status === 'success') {
			$('[data-item-id="' + itemId + '"]').remove();
			$('#' + itemId).fadeOut('normal', function() {
				$(this).remove();
			});
			viewmodel.totalFotos($('[name="uploadedFiles[]"]').length);
		}
	}, 'json');
}

$(function() {
	var errorHandler = function(event, id, fileName, reason) {
		qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
	};
	var uploader = new qq.FineUploader({
		element : $('#uploadBtn')[0],
		// debug: true,
		request : {
			endpoint : uploadFilesUri
		},
		validation : {
			allowedExtensions : ['jpeg', 'jpg', 'png', 'gif'],
			sizeLimit : 5.243e+6 // 5MB
		},
		text : {
			uploadButton : '<i class="icon-folder-open icon-white"></i> Selecionar fotos do meu computador',
			dragZone : 'Arraste e solte suas fotos aqui',
			cancelButton : 'cancelar',
			retry : 'Tentar novamente',
			formatProgress : '{percent}% de {total_size}',
			failUpload : 'Falha ao enviar',
			waitingForResponse : 'Processando...'
		},
		template : '<div class="qq-uploader">' +
				   '<pre class="qq-upload-drop-area span12"><span>{dragZoneText}</span></pre>' +
				   '<div class="qq-upload-button flat-butt flat-info-butt" style="width: auto;">{uploadButtonText}</div>' +
				   '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
				   '</div>' +
				   '<div class="qq-upload-list" style="margin-top: 10px; text-align: center;">  <ul id="customUploadList" class="unstyled"></ul> </div>',
		classes : {
			fail : 'alert alert-error'
		},
		callbacks : {
			onError : errorHandler,
			onComplete : function(id, name, response) {
				var info = response.uploadInfo;
				if (response.uploadInfo) {
					uniqid = Date.now();
					var uploadedFile = response.uploadInfo[0].relative_path;
					var element = $('<input type="hidden" name="uploadedFiles[]">').val(uploadedFile).attr('data-item-id', uniqid).appendTo('#ajaxUploaded');
					var thumb = $('<img />').attr({
						'src' : uploadDir + response.uploadInfo[0].relative_path,
						'data-image-for' : uniqid,
						'data-relative-path' : response.uploadInfo[0].relative_path,
					});
					var $customListEl = $('<li />').attr('id', uniqid).append(thumb);
					    $customListEl.prepend('<a href="#" class="rm" data-rm-id="' + uniqid + '">&times;</a>');
					$('#customUploadList').append($customListEl);
					viewmodel.totalFotos($('[name="uploadedFiles[]"]').length);
					$('.qq-upload-success').hide();

				} else {
					$('#uploadBtn li').eq(id).removeClass('alert-success').addClass('alert-error');
				}
			}
		},
		failedUploadTextDisplay : {
			mode : 'custom',
			maxChars : 80,
			responseProperty : 'error',
			enableTooltip : true
		},
		deleteFile : {
			enabled : false
		}
	});

	$(".inlineModal").on('click', function(e) {
		e.preventDefault();
		var el = $(this).attr('href');
		FB.getLoginStatus(function(response) {
			if (response.status === "connected") {
				$.colorbox({
					fixed : true,
					inline : true,
					width : "80%",
					height : "90%",
					'href' : el
				});
			} else {
				loginFacebook(function() {
					setTimeout(function() {
						$(".inlineModal").click();
					}, 1000);
				});
			}

		});
	});

	$('#services').slimScroll({
		height : '320px',
		railVisible : true,
		alwaysVisible : true,
	});

	 $('.qq-upload-list, #facebook-list-box').slimScroll({
		 height : '130px',
		 railVisible : true,
		 alwaysVisible : true,
	 });

	$('.rm').live('click', function(e) {
		e.preventDefault();
		var rmId = $(this).attr('data-rm-id');
		removeItemFromUploadedList(rmId);
	});

	$('[name="servicos[]"]').on('change', function() {
		var $servicos = $('[name="servicos[]"]:checked');
		var totalSelecionado = 0;
		$.each($servicos, function(key, obj) {
			totalSelecionado += parseFloat(obj.getAttribute('data-preco'));
		});
		viewmodel.custoServicosSelecionados(totalSelecionado);
	});

	$('#logoutFace').on('click', function(e) {
		e.preventDefault();
		logoutFacebook(function(){
			viewmodel.facebookPhotos('');
			loginFacebook();
		});
	});

	ko.applyBindings(viewmodel);

});
