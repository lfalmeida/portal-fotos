<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Servicos extends Admin_Controller
{
	/**
	 *
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model('servico_dao_model', 'servicoRepository');
	}

    /**
     *
     *
     **/
	public function index() {
        $this->gerenciar();
	}


	/**
	 *  	  
	 */
	public function cadastrar() {
		if($this->input->post('submit')) {
			$servico = new Servico();
			$servico->setNome($this->input->post('nome', true))
					->setDescricao($this->input->post('descricao', true))
					->setPreco($this->input->post('preco', true)); 
			try {
				$result = $this->servicoRepository->save($servico);
				if($result) {
					$message = array(
						'type' => 'success',
						'text' => 'Cadastro realizado com sucesso.'
					);
                    
                    Logger::info('Novo serviço cadastrado: ' . $servico->getNome());
                    
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar();
                    return true;
				}				
			} catch(Exception $e) {
				echo $e->getMessage();
			}
		}
 		$this->load->view('public/servicos/cadastrar');		
	} 

    /**
     * 
     */
    public function gerenciar($pag = 0) {
        $this->load->library('pagination');
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url() . 'servicos/gerenciar/';
        $config['total_rows'] = count($this->servicoRepository->getAll());
        $config['per_page'] = 12;
        $this->pagination->initialize($config);
        $paginacao = $this->pagination->create_links();
        $limit = $config['per_page'];
        $offset = $this->uri->segment(3);
        $order = 'id DESC';
        $servicos = $this->servicoRepository->getAll(array(
            'limit' => $limit,
            'offset' => $offset,
            'order' => $order
                )
        );
        $this->load->vars('servicos', $servicos);
        $this->load->vars('paginacao', $paginacao);
        $this->load->vars('currentPage', floor(($this->uri->segment(3) / $config['per_page']) + 1));
        $this->load->view('public/servicos/gerenciar');
    }

    /**
     * 
     * 
     */
    public function alterar($servicoId = false) {        
        if($this->input->post('submit')) {
            $servico = new Servico();
            
            $servico->setId($this->input->post('servico_id'))
                    ->setNome($this->input->post('nome', true))
                    ->setDescricao($this->input->post('descricao', true))
                    ->setPreco($this->input->post('preco', true)); 
            try {
                $result = $this->servicoRepository->save($servico);
                if($result) {
                    $message = array(
                        'type' => 'success',
                        'text' => 'Cadastro alterado com sucesso.'
                    );
                    
                    Logger::info('Serviço alterado: ' . $servico->getNome());
                    
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar();
                    return true;
                }               
            } catch(Exception $e) {
                echo $e->getMessage();
            }
        }
        $servico = $this->servicoRepository->find($servicoId);
        $this->load->vars('servico', $servico);
        $this->load->view('public/servicos/alterar'); 
    }
 

    /**
     * 
     * 
     */
    public function excluir($servicoId = false) {
        if($this->input->post('submit')) {
            $servicoId = $this->input->post('servico_id');
            try {
                $result = $this->servicoRepository->excluir($servicoId);
                 if($result) {
                    $message = array(
                        'type' => 'success',
                        'text' => 'Cadastro excluído com sucesso.'
                    );
                    $this->load->vars('message', (object) $message);
                    
                    Logger::info('Serviço excluído. id: ' . $servicoId );
                    
                    $this->gerenciar(); 
                    return true; 
                 }                
            } catch (Exception $e) {
                    $message = array(
                        'type' => 'error',
                        'text' => $e->getMessage()
                    );
                    $this->load->vars('message', (object) $message);
                    $this->gerenciar(); 
                    return true;                 
            }
            
        }
        $servico = $this->servicoRepository->find(($servicoId));
        $this->load->vars('servico', $servico);
        $this->load->view('public/servicos/confirmar_exclusao');
    }

}
