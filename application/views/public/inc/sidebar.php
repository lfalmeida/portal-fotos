<div class="span3">
	<div class="bs-docs-sidebar sidebar-nav-fixed">
		<ul class="nav nav-list cp-sidenav ">
			<li>
				<a href="<?php echo site_url('admin/clientes/cadastrar')?>"><i class="icon-chevron-down"></i> Clientes</a>
				<ul class="nav nav-list">
					<li>
						<a href="<?php echo site_url('admin/clientes/cadastrar')?>"><i class="icon-plus"></i> Cadastrar cliente</a>
					</li>
					<li>
						<a href="<?php echo site_url('admin/clientes/gerenciar')?>"><i class="icon-list"></i> Gerenciar cliente</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="<?php echo site_url('usuarios/cadastrar')?>"><i class="icon-chevron-down"></i> Usuários</a>
				<ul class="nav nav-list">
					<li>
						<a href="<?php echo site_url('usuarios/cadastrar')?>"><i class="icon-plus"></i> Cadastrar usuário</a>
					</li>
					<li>
						<a href="<?php echo site_url('usuarios/gerenciar')?>"><i class="icon-list"></i> Gerenciar  usuários</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="<?php echo site_url('servicos/cadastrar')?>"><i class="icon-chevron-down"></i> Servicos</a>
				<ul class="nav nav-list">
					<li>
						<a href="<?php echo site_url('servicos/cadastrar')?>"><i class="icon-plus"></i> Cadastrar serviço</a>
					</li>
					<li>
						<a href="<?php echo site_url('servicos/gerenciar')?>"><i class="icon-list"></i> Gerenciar  serviços</a>
					</li>
				</ul>
			</li>			
		</ul>
	</div>
</div>

