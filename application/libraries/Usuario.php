<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Usuário do sistema.
 * Pode ser administrador ou cliente,
 * e diferença é determinada por uma flag admin = true || false;
 *
 */
class Usuario {

	private $id;
	private $facebookId;
	private $nome;
	private $sobrenome;
	private $email;
	private $status;
	private $admin;
	private $senha;

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setFacebookId($facebookId)
	{
		$this->facebookId = $facebookId;
		return $this;
	}

	public function getFacebookId()
	{
		return $this->facebookId;
	}

	public function setNome($nome)
	{
		$this->nome = $nome;
		return $this;
	}

	public function getNome()
	{
		return $this->nome;
	}

	public function setSobrenome($sobrenome)
	{
		$this->sobrenome = $sobrenome;
		return $this;
	}

	public function getSobrenome()
	{
		return $this->sobrenome;
	}


    public function getNomeCompleto() {
        return $this->nome . ' ' . $this->sobrenome;
    }

	public function setEmail($email)
	{
		$this->email = $email;
		return $this;

		// if (!empty($email) AND filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
		//	 $this->email = $email;
		//   return $this;
		// } else {
	    //	 throw new Exception('O email informado é inválido');
		//	 return false;
		// }
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	public function getStatus()
	{
		return $this->status;
	}


	public function setSenha($senha)
	{
		$this->senha = $senha;
		return $this;
	}

	/**
	 *
	 */
	public function getSenha($applyHash = false)
	{
		return $applyHash ? md5($this->senha) : $this->senha;
	}

	/**
	 *
	 */
	public function setAdminPerm($adminPerm)
	{
		$this->admin = $adminPerm;
		return $this;
	}

	/**
	 *
	 */
	public function isAdmin()
	{
		return $this->admin;
	}

}
