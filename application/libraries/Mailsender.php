<?php


/**
*
*/
class Mailsender
{

	private $message,
			$subject,
			$sender,
			$receiver,
			$bcc;

	/**
	 * Método setter da propriedade $message
	 *
	 *
	 */
	public function setMessage ($message)
	{
	    $this->message = $message;
	    return $this;
	}

	/**
	 * Método setter da propriedade $message
	 *
	 *
	 */
	public function getMessage ()
	{
	    return $this->message;
	}

	/**
	 * Método setter da propriedade $subject
	 *
	 *
	 */
	public function setSubject ($subject)
	{
	    $this->subject = $subject;
	    return $this;
	}

	/**
	 * Método setter da propriedade $subject
	 *
	 *
	 */
	public function getSubject()
	{
	    return $this->subject;
	}

	/**
	 * Método setter da propriedade $sender
	 *
	 *
	 */
	public function setSender ($sender)
	{
	    $this->sender = $sender;
	    return $this;
	}

	/**
	 * Método setter da propriedade $sender
	 *
	 *
	 */
	public function getSender ()
	{
	    return $this->sender;
	}

	/**
	 * Método setter da propriedade $receiver
	 *
	 *
	 */
	public function setReceiver ($receiver)
	{
	    $this->receiver = $receiver;
	    return $this;
	}

	/**
	 * Método setter da propriedade $receiver
	 *
	 *
	 */
	public function getReceiver ()
	{
	    return $this->receiver;
	}


	/**
	 * Método setter da propriedade $bcc
	 *
	 *
	 */
	public function setBcc ($bcc)
	{
	    $this->bcc = $bcc;
	    return $this;
	}


	/**
	 * Método setter da propriedade $bcc
	 *
	 *
	 */
	public function getBcc()
	{
	    return $this->bcc;
	}



	public function send()
	{
        $headers  = "MIME-Version: 1.1\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: " . $this->getSender() . "\n";
        $headers .= $this->getBcc() ? "Bcc: contato@plavision.com.br \n" : '';
        $headers .= "Return-Path: " . $this->getSender() . "\n";
        $headers .= "Reply-To: " . $this->getSender() . "\n";

        if(mail($this->getReceiver(), $this->getSubject(), $this->getMessage(), $headers, "-r". $this->getSender()))
        {
        	return true;
        }
        return false;
	}

}

