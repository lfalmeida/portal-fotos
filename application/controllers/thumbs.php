<?php

/**
 *
 *
 *
 */
class Thumbs extends CI_Controller {

	/**
	 *
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
	}

	/**
	 *
	 *
	 *
	 */
	public function view($width, $height)
	{

            $img = $this->uri->slash_segment(5) .
                   $this->uri->slash_segment(6) .
                   $this->uri->slash_segment(7) .
                   $this->uri->slash_segment(8) .
                   $this->uri->segment(9);
 
            $image_info = getimagesize($img);

            $original_width  = $image_info['0'];
            $original_height = $image_info['1'];

            if($original_width < $width  ) {
                $width  = $original_width;
                $height = $original_height;
            }

            $config['source_image']	= $img;
            $config['width'] = $width;
            $config['height'] = $height;
            $config['dynamic_output'] = true;

            $this->image_lib->initialize($config);

            $this->image_lib->resize();

	}

}
