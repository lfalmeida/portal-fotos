<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *
 *
 *
 **/
class Retorno extends Public_Controller
{

	/**
	 *
	 *
	 */
	public function __construct() {
		parent::__construct();
        $this->load->library('PagSeguroLibrary/PagSeguroLibrary');
	}

    /**
     * Url que recebe o usuário de volta após ter realizado o pagamento no
     * PagSeguro
     *
     * ---- Importante ---
     * Para que este método funcione, é necessário que seja configurada no
     * painel do pagseguro a opção de Redirecionamento com o código da transação.
     *
     * O PagSeguro irá enviar como parâmetro, via GET, o código da transação que
     * foi gerado. Esse código será utilizado para exibir ao comprador, os dados da transação
     *
     * O nome do parâmetro esperado pelo método é 'transaction_id', e precisa ser
     * configurado no pagseguro.
     *
     * https://pagseguro.uol.com.br/integracao/pagina-de-redirecionamento.jhtml
     *
     **/
	public function index() {

        $transactionId = (isset($_GET['transaction_id']) && trim($_GET['transaction_id']) !== "" ?
            trim($_GET['transaction_id']) : null);

         if($transactionId) {

             Logger::info('Pagseguro redirecionou o cliente de volta com o código da transação: ' . $transactionId);

             $transaction = $this->searchTransactionByCode($transactionId);
             $result = $this->updatePedidoWithPagSeguroTransaction($transaction);
 
             $this->transaction = $transaction;

             $this->messageCheckout();
             return true;
         }


	}


    public function messageCheckout() {
        $transaction = $this->transaction;
        $pedido = $this->pedidoRepository->find($transaction->getReference());

        $statusCode = $transaction->getStatus()->getValue();
        $status = new stdClass();
        $status->code = $statusCode;
        switch ($statusCode) {
            case '1':
                $status->text = 'Aguardando Pagamento';
                break;
            case '2':
                $status->text = 'Em análise';
                break;
            case '3':
                $status->text = 'Pagamento Aprovado';
                break;
            case '7':
                $status->text = 'Pagamento Cancelado';
                break;
        }
        $paymentCode = $transaction->getPaymentMethod()->getCode()->getValue();
        if($paymentCode > 100 && $paymentCode < 200 ) $tipoPagamento = 'Cartão de crédito';
        if($paymentCode > 200 && $paymentCode < 300 ) $tipoPagamento = 'Boleto bancário';
        if($paymentCode > 300 && $paymentCode < 400 ) $tipoPagamento = 'Débito online';
        if($paymentCode > 700 && $paymentCode < 800 ) $tipoPagamento = 'Débito online';

        Logger::info('Exibindo para o cliente a tela de retorno com o status: ' . $status->text);

        $this->load->vars('tipoPagamento', $tipoPagamento);
        $this->load->vars('status', $status);
        $this->load->vars('code',  $transaction->getCode());
        $this->load->vars('valor', number_format($transaction->getGrossAmount(), '2', ',', '.' ));
        $this->load->vars('pedido', $pedido);
        $this->load->view('public/pedidos/retorno_pagseguro');
    }

    /**
     * recebe posts do pagseguro quando ocorrem mudanças em uma transação
     */
    public function notificacoes() {
        $code = (isset($_POST['notificationCode']) && trim($_POST['notificationCode']) !== "" ?
            trim($_POST['notificationCode']) : null);
        $type = (isset($_POST['notificationType']) && trim($_POST['notificationType']) !== "" ?
            trim($_POST['notificationType']) : null);

        if ($code && $type) {
            $notificationType = new PagSeguroNotificationType($type);
            $strType = $notificationType->getTypeFromValue();
            switch ($strType) {
                case 'TRANSACTION':

                    Logger::info('Recebido post do pagseguro com o código: ' . $code);
                    $this->transactionNotification($code);

                    //-------------------- DEBUG -------------------------------------
                    // temporariamente enviar os dados das transações para debug
                    if(ENV === 'testing') {
                        $this->load->library('email');
                        $this->email->from('postmaster@teste.com', 'Teste Integração');
                        $this->email->to('powerpuffsk8@gmail.com');
                        $this->email->subject('post pagseguro');
                        $this->email->message('Post PagSeguro <br/>' . 'Recebido post do pagseguro com o código: ' . $code);
                        $this->email->send();
                    }
                    //-------------------- DEBUG -------------------------------------
                    break;
                default:
                    LogPagSeguro::error("Unknown notification type [" . $notificationType->getValue() . "]");
            }
        } else {
            LogPagSeguro::error("Invalid notification parameters.");
        }
    }


    /**
     *
     * Recebe um código de notificação que é usuado para obter
     * a transação e chama o método de atualização do pedido
     *
     */
    private function transactionNotification($notificationCode) {

        $credentials = new PagSeguroAccountCredentials(
            $this->config->item('pagseguroAccount'),
            $this->config->item('pagseguroToken')
        );
        try {
            $transaction = PagSeguroNotificationService::checkTransaction($credentials, $notificationCode);
            $this->updatePedidoWithPagSeguroTransaction($transaction);
        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }


    /**
     * Retorna uma transação do pagseguro a partir do seu código
     *
     */
    public function searchTransactionByCode($transaction_code) {
        try {
            $credentials = new PagSeguroAccountCredentials(
                $this->config->item('pagseguroAccount'),
                $this->config->item('pagseguroToken')
            );
            return PagSeguroTransactionSearchService::searchByCode($credentials, $transaction_code);
        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }


    /**
     *
     *
     */
    private function updatePedidoWithPagSeguroTransaction(PagSeguroTransaction $transaction) {

        $this->load->model('pedido_dao_model', 'pedidoRepository');
        $pedidoId = $transaction->getReference();
        $data = array();
        /*
         * STATUS_PAGO = 3
         * transação foi paga pelo comprador e o
         * PagSeguro já recebeu uma confirmação
         * da instituição financeira responsável
         */
        define('STATUS_PAGO', 3);
        if( $transaction->getStatus()->getValue() == STATUS_PAGO ) {
            $data['status'] = 'E'; // mudar status para pagamento efetuado
            Logger::info('Atualizando o pedido para "PAGAMENTO EFETUADO", com os dados recebidos do pagseguro');
            $this->enviarNotificacaoPagamentoEfetuado($pedidoId);
        }

        $data['pagseguro_transaction_code'] = $transaction->getCode();
        $this->db->where('id', $pedidoId);
        return $this->db->update('pedidos', $data);
    }


    /**
     * 
     * 
     */
    public function enviarNotificacaoPagamentoEfetuado($pedidoId) {
        $this->load->library('email');
        $this->config->load('notification');
        
        $this->load->model('pedido_dao_model', 'pedidoRepository');
        $this->load->model('usuario_dao_model', 'usuarioRepository');
                
        $pedido = $this->pedidoRepository->find($pedidoId);
        $cliente = $this->usuarioRepository->find($pedido->getCliente());
 
        $this->load->vars('pedido', $pedido);
        $this->load->vars('cliente', $cliente);
  
        $message = $this->load->view('email/pagamento_aprovado', '', TRUE);
   
        // enviar para o cliente
        $this->email->from($this->config->item('postmaster_address'));
        $this->email->to($cliente->getEmail());
        $this->email->subject('Pagamento aprovado');
        $this->email->message($message);    
        
        if($this->email->send()) {
            Logger::info('Enviado email de pagamento recebido. Cliente: ' .
                          $cliente->getNomeCompleto() . 
                          '. Pedido id: ' . $pedido->getId() );
        }
                    
    }
    


}
