<?php


class Logger {

    private static $CI;

    /**
     *
     */
    static function init() {
        self::$CI =& get_instance();
    }

    /**
     *
     */
    static public function info($message) {
        self::saveMessage($message, 'INFO');
    }

    /**
     *
     */
    static public function warning($message) {
        self::saveMessage($message, 'WARN');
    }

    /**
     *
     */
    static public function error($message) {
        self::saveMessage($message, 'ERROR');
    }

    /**
     *
     */
    static private function saveMessage($message, $type) {

        if(!session_id()) session_start();

        $username    = isset($_SESSION['name'])        ? $_SESSION['name']        : '';
        $userId      = isset($_SESSION['user_id'])     ? $_SESSION['user_id']     : '';
        $facebook_id = isset($_SESSION['facebook_id']) ? $_SESSION['facebook_id'] : '';
        $isAdmin     = isset($_SESSION['is_admin'])    ? $_SESSION['is_admin']    : '';

        $data = array(
            'message'     => $message,
         // desabilitando a gravação do tipo de mensagem
         //   'type'        => $type,
            'username'    => $username,
            'user_id'     => $userId,
            'facebook_id' => $facebook_id,
            'admin'       => $isAdmin
        );

        self::$CI->db->insert('log', $data);

    }

}

Logger::init();
